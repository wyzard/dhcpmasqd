/* dhcpmasqd.c - main program
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <confuse.h>
#include <errno.h>
#include <getopt.h>
#include <glib.h>
#include <libdaemon/dfork.h>
#include <libdaemon/dlog.h>
#include <libdaemon/dpid.h>
#include <libnet.h>
#include <pcap.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "event-queue.h"
#include "event-source.h"
#include "interfaces.h"
#include "leases.h"
#include "logging.h"
#include "packets.h"
#include "pcap-source.h"
#include "routing.h"
#include "settings.h"
#include "signal-source.h"
#include "transactions.h"

/* The name of the configuration file */
static gchar const default_conf_filename[] = DHCPMASQD_CONFIG_FILE;
static gchar const *conf_filename;

/* The application's main loop */
static GMainLoop *main_loop;

/* Flag indicating whether we've been requested to kill a running instance
   rather than starting a new one */
static gboolean kill_running_instance = FALSE;

/* Flag indicating whether we're running in daemon mode */
static gboolean be_daemon = TRUE;

static gint provide_service();
static gboolean signal_source_callback(gint signal, gpointer data);
static gboolean leasedb_refresh_callback(gpointer data);
static void parse_options(int argc, gchar **argv);
static void print_help();
static void print_version();

int main (int argc, char **argv) {
  /* Set identification string for both pidfile and syslog */
  daemon_pid_file_ident = daemon_log_ident = daemon_ident_from_argv0(argv[0]);

  g_set_prgname(argv[0]);
  parse_options(argc, argv);
  load_settings(conf_filename);

  if (kill_running_instance) {
    gint kill_result = daemon_pid_file_kill_wait(SIGINT, 5);
    if (kill_result < 0)
      log_fatal_perror(LOG_ERR, "Failed to kill running instance", 1);
    else return 0;
  }

  /* Become a daemon if desired */
  if (be_daemon) {
    pid_t const existing_pid = daemon_pid_file_is_running();

    if (existing_pid >= 0)
      log_fatal_msg(LOG_ERR, "Already running on PID file %u", 1, existing_pid);

    pid_t const child_pid = daemon_fork();
    if (child_pid < 0)
      log_fatal_perror(LOG_ERR, "Failed to daemonize", 1);
    else if (child_pid) {  // Parent
      return 0;
    }
    else {  // Child
      return provide_service();
    }
  }
  else  // No daemon; just run without forking
    return provide_service();
}

gint provide_service() {
  /* Close any leftover file handles, for safety */
  daemon_close_all(-1);

  /* Create a pidfile */
  if (daemon_pid_file_create() < 0)
    log_fatal_perror(LOG_ERR, "Failed to create pidfile", 1);

  /* Set up handling of interesting signals */
  if ((signal_source_handle_signal(SIGINT) < 0) ||
      (signal_source_handle_signal(SIGTERM) < 0)) {
    log_perror(LOG_WARNING, "Failed to set up signal handling");
  }
  signal_source_add(signal_source_callback, NULL);

  /* Initialize the networking, transaction and lease subsystems */
  interfaces_init(ifn_internal, ifn_external);
  transactions_init();
  leases_init();

  /* Arrange for the lease database to be refreshed periodically */
  g_timeout_add_seconds(leasedb_refresh_interval,
			leasedb_refresh_callback, NULL);

  /* Main loop to read packets */
  log_msg(LOG_NOTICE, "Beginning DHCP masquerading service");
  main_loop = g_main_loop_new(NULL, FALSE);
  g_main_loop_run(main_loop);
  g_main_loop_unref(main_loop);
  main_loop = NULL;
  log_msg(LOG_NOTICE, "Stopped DHCP masquerading service");

  /* Tear down the networking, transaction, and lease subsystems */
  leases_cleanup();
  transactions_cleanup();
  interfaces_cleanup();

  /* Remove the pidfile */
  daemon_pid_file_remove();

  return 0;
}

static gboolean signal_source_callback(gint signal, gpointer data) {
  switch (signal) {
  case SIGINT:
    log_msg(LOG_NOTICE, "Received SIGINT; exiting");
    g_main_loop_quit(main_loop);
    break;

  case SIGTERM:
    log_msg(LOG_NOTICE, "Received SIGTERM; exiting");
    g_main_loop_quit(main_loop);
    break;

  default:
    log_msg(LOG_WARNING, "Received unexpected signal %u; ignoring", signal);
    break;
  }

  return TRUE;  // Continue checking this source
}

static gboolean leasedb_refresh_callback(gpointer data) {
  refresh_lease_database();

  return TRUE;  // Continue checking this source
}

void parse_options(int argc, char **argv) {
  /* Valid long-options list */
  static struct option const getopt_options[] = {
    { "help", no_argument, NULL, 'h' },
    { "debug", no_argument, NULL, 'd' },
    { "config", required_argument, NULL, 'c' },
    { "version", no_argument, NULL, 'V' },
    { NULL, 0, NULL, 0 }
  };

  /* Load defaults for string options that might be affected */
  conf_filename = strdup(default_conf_filename);

  while (1) {
    gint opt = getopt_long(argc, argv, "hkdc:V", getopt_options, NULL);

    if (opt == -1) break;

    switch (opt) {
    case 'h':  // Help
      print_help();
      exit(EXIT_SUCCESS);

    case 'k':  // Kill running instance
      kill_running_instance = TRUE;
      break;

    case 'd':  // Debug mode
      be_daemon = FALSE;
      break;

    case 'c':  // Config file
      g_free((gchar *) conf_filename);
      conf_filename = strdup(optarg);
      break;

    case 'V':  // Version
      print_version();
      exit(EXIT_SUCCESS);

    case '?':  // Unrecognized option
      print_help();
      exit(EXIT_FAILURE);

    default:
      /* We should never get here - known options have their handlers, and
	 getopt returns '?' for unknown options */
      fprintf(stderr, "%s: %s line %u, unhandled return value from getopt_long()\n",
	      g_get_prgname(), __FILE__, __LINE__);
      exit(1);
    }
  }
}

void print_help() {
  printf("\
Usage: %s [options]\n\
\n\
  -h, --help                      Show this help\n\
  -k                              Kill a running instance\n\
  -d, --debug                     Debug mode (don't become a daemon)\n\
  -c, --config=FILE               Read configuration from FILE\n\
                                    (Default:  %s)\n",
	 g_get_prgname(), default_conf_filename);
}

void print_version() {
  printf("%s %s\n", PACKAGE, VERSION);
  printf("\
Copyright (C) %s Michael B. Paul\n\
License GPLv2+: GNU GPL version 2 or later <http://gnu.org/licenses/gpl.html>\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\n", "2009");
}
