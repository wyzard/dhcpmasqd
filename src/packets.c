/* packets.c - subsystem for examining and manipulating DHCP packets
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <glib.h>
#include <net/ethernet.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "dhcp.h"
#include "logging.h"
#include "packets.h"
#include "pcap-source.h"
#include "util.h"

#define BYTE_OFFSET(ptr, ofs) ((guint8 *)(ptr) + (ofs))
#define IP_HDR_BYTES(ihl) ((ihl) * (sizeof(guint32) / sizeof(guint8)))
#define IP_HDR_WORDS(ihl) ((ihl) * (sizeof(guint32) / sizeof(guint16)))

#define SKIP_ETHERNET_HEADER(hdr_ethernet) ((struct iphdr *) BYTE_OFFSET((hdr_ethernet), sizeof(*(hdr_ethernet))))
#define SKIP_IP_HEADER(hdr_ip) ((struct udphdr *) BYTE_OFFSET((hdr_ip), IP_HDR_BYTES((hdr_ip)->ihl)))
#define SKIP_UDP_HEADER(hdr_udp) ((struct dhcp_packet *) BYTE_OFFSET((hdr_udp), sizeof(*(hdr_udp))))

/* A portion of a DHCP packet that contains a list of tagged options */
struct option_area {
  guint8 *bytes;
  gsize length;
};

/* The three places in a DHCP packet where options may be found */
enum option_area_name {
  OPTIONS_MAIN,  // Primary options area at the end of the packet
  OPTIONS_IN_FILE,   // Options stored in packet's "file" field
  OPTIONS_IN_SNAME,  // Options stored in packet's "sname" field
  NUM_OPTION_AREAS
};

struct option_iterator {
  guint8 *option_area_end;  // End of option data in this option area

  /* Per-option data that changes with each call to option_iterator_next() */
  guint8 *option_bytes;  // Whole option (incl. tag and length)
  gsize option_length;   // Whole option (incl. tag and length)
  guint8 option_tag;
  guint8 *value_bytes;  // excl. tag and length
  gsize value_length;   // excl. tag and length

  guint8 *next_option;  // The option that follows this one, if any
};

struct _DhcpPacket {
  GByteArray *raw_frame;  // The raw Ethernet frame, in wire format
  struct ether_header *hdr_ethernet;  // Ethernet header portion of packet
  struct iphdr *hdr_ip;  // IP header portion of packet
  struct udphdr *hdr_udp;  // UDP header portion of packet
  struct dhcp_packet *payload;  // DHCP payload portion of packet
  guint8 *payload_end;  // Points to first byte after end of payload

  /* Pointers to option areas within this packet */
  /* Areas not used in this packet are represented by NULL pointers */
  struct option_area option_areas[NUM_OPTION_AREAS];

  guint8 message_type;  // From the (mandatory) DHO_DHCP_MESSAGE_TYPE option
};

/* static functions */
static gboolean init_and_verify(DhcpPacket *packet, GError **error);
static void option_iterator_init(struct option_iterator *iterator,
				 struct option_area const *option_area);
static gboolean option_iterator_next(struct option_iterator *iterator,
				     GError **error);
static guint16 ip_checksum(struct iphdr const *hdr);
static guint16 udp_checksum(struct udphdr const *hdr,
			      guint32 ip_saddr, guint32 ip_daddr);
static guint32 checksum_core(guint16 const *data, guint count);
static guint16 checksum_final(guint32 presum);

GQuark dhcpmasqd_packet_error_quark() {
  return g_quark_from_static_string("dhcpmasqd-packet-error-quark");
}

/* packet_create()
 *
 * Creates a DhcpPacket structure from a raw Ethernet frame.
 *
 * Returns a pointer to a newly-allocated DhcpPacket structure if successful,
 * or NULL if the given bytes are not a valid DHCP packet.
 * This function takes ownership of the given GByteArray.
 */
DhcpPacket *packet_create(GByteArray *raw_frame, GError **error) {
  g_return_val_if_fail(raw_frame != NULL, NULL);
  g_return_val_if_fail(error == NULL || *error == NULL, NULL);

  DhcpPacket *packet = g_slice_new(DhcpPacket);
  packet->raw_frame = raw_frame;
  packet->payload_end = packet->raw_frame->data + packet->raw_frame->len;

  /* Initialize the DhcpPacket structure and return it if it's valid */
  if (init_and_verify(packet, error)) {
    return packet;
  }
  else {  // Something didn't check out, so discard the packet
    g_byte_array_free(packet->raw_frame, TRUE);
    g_slice_free(DhcpPacket, packet);
    return NULL;
  }
}

/* packet_destroy()
 *
 * Releases resources associated with a DhcpPacket
 */
void packet_destroy(DhcpPacket *packet) {
  g_return_if_fail(packet != NULL);

  g_byte_array_free(packet->raw_frame, TRUE);
  g_slice_free(DhcpPacket, packet);
}

/* packet_get_xid()
 *
 * Returns the transaction ID of the DHCP packet.
 */
guint32 packet_get_xid(DhcpPacket const *packet) {
  g_return_val_if_fail(packet != NULL, 0);

  return g_ntohl(packet->payload->xid);
}

/* packet_get_message_type()
 *
 * Returns the "message type" of the DHCP packet.
 */
guint8 packet_get_message_type(DhcpPacket const *packet) {
  g_return_val_if_fail(packet != NULL, 0);

  return packet->message_type;
}

/* packet_get_lease_addr()
 *
 * Returns the IP address granted by a DHCP lease - really just the
 * "yiaddr" field
 *
 * Doesn't make sense to use on DHCPDISCOVER, DHCPINFORM, or DHCPRELEASE
 */
guint32 packet_get_lease_addr(DhcpPacket const *packet) {
  g_return_val_if_fail(packet != NULL, 0);

  return g_ntohl(packet->payload->yiaddr.s_addr);
}

/* packet_get_client_addr()
 *
 * Returns the IP address of the client making the DHCP request - the
 * "ciaddr" field
 *
 * Only makes sense on DHCPRELEASE
 */
guint32 packet_get_client_addr(DhcpPacket const *packet) {
  g_return_val_if_fail(packet != NULL, 0);

  return g_ntohl(packet->payload->ciaddr.s_addr);
}

/* packet_get_client_hwaddr
 *
 * Returns the hardware (e.g. MAC) address of the client making the request.
 */
GByteArray *packet_get_client_hwaddr(DhcpPacket const *packet) {
  g_return_val_if_fail(packet != NULL, NULL);

  GByteArray *client_hwaddr =
    g_byte_array_sized_new(sizeof(packet->payload->chaddr));
  g_byte_array_append(client_hwaddr, packet->payload->chaddr,
		      sizeof(packet->payload->chaddr));
  return client_hwaddr;
}

/* packet_get_broadcast_flag()
 *
 * Returns the flag that indicates whether responses to the client should be
 * sent via broadcast instead of unicast.  (This is referred to as the
 * "broadcast bit" in RFC 2131, but this function is meant to abstract away the
 * fact that it's a bit in the packet's flags field, so "flag" is a more
 * appropriate term.)
 */
gboolean packet_get_broadcast_flag(DhcpPacket const *packet) {
  g_return_val_if_fail(packet != NULL, FALSE);

  return g_ntohs(packet->payload->flags) & BOOTP_BROADCAST;
}

/* packet_set_broadcast_flag()
 *
 * Sets the flag that indicates that responses to the client should be sent
 * via broadcast instead of unicast.  Also see packet_get_broadcast_flag().
 */
void packet_set_broadcast_flag(DhcpPacket *packet, gboolean broadcast_flag) {
  g_return_if_fail(packet != NULL);

  guint16 flags = g_ntohs(packet->payload->flags);

  if (broadcast_flag)
    flags |=  BOOTP_BROADCAST;
  else
    flags &= ~BOOTP_BROADCAST;

  packet->payload->flags = g_htons(flags);
}

/* packet_has_split_options()
 *
 * Checks whether a packet contains any RFC 3396 split options.  If so, then
 * the sender implements RFC 3396, and believes that the recipient does too, so
 * it should be safe to pass TRUE for the split_allowed parameter when calling
 * packet_add_option().
 *
 * If an error is encountered while processing the packet's options,
 * it will be reported via the GError parameter, and FALSE will be returned.
 */
gboolean packet_has_split_options(DhcpPacket const *packet, GError **error) {
  g_return_val_if_fail(packet != NULL, FALSE);
  g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

  gboolean option_tags_seen[G_MAXUINT8];
  for (int i = 0; i < G_N_ELEMENTS(option_tags_seen); i += 1)
    option_tags_seen[i] = FALSE;

  for (guint i = 0; i < NUM_OPTION_AREAS; i += 1) {
    struct option_area const *option_area = &(packet->option_areas[i]);
    struct option_iterator iterator;
    option_iterator_init(&iterator, option_area);

    /* Iterate over the options in the packet */
    GError *iteration_error = NULL;
    while (option_iterator_next(&iterator, &iteration_error)) {
      if (iterator.option_tag == DHO_PAD || iterator.option_tag == DHO_END) {
	/* The fixed-length options don't count since they have no value */
	continue;
      }
      else if (option_tags_seen[iterator.option_tag]) {
	/* If the same option tag occurs twice, it's a split option */
	return TRUE;
      }
      else {
	/* Keep track of which tags have been seen already */
	option_tags_seen[iterator.option_tag] = TRUE;
      }
    }

    if (iteration_error != NULL) {
      g_propagate_error(error, iteration_error);
      return FALSE;
    }
  }

  return FALSE;  // If we get here then no split options were found
}

/* packet_get_option()
 *
 * Retrieves the value of an option from the packet.  If the option occurs
 * multiple times in the packet, the values will be concatenated in accordance
 * with RFC 3396.  If the requested option does not occur at all, NULL will be
 * returned.
 *
 * The caller is responsible for freeing the returned GByteArray.
 *
 * If an error is encountered while processing the packet's options,
 * it will be reported via the GError parameter, and NULL will be returned.
 */
GByteArray *packet_get_option(DhcpPacket const *packet, guint8 tag, GError **error) {
  g_return_val_if_fail(packet != NULL, NULL);
  g_return_val_if_fail(error == NULL || *error == NULL, NULL);

  GByteArray *option_value = NULL;

  for (guint i = 0; i < NUM_OPTION_AREAS; i += 1) {
    struct option_area const *option_area = &(packet->option_areas[i]);
    struct option_iterator iterator;
    option_iterator_init(&iterator, option_area);

    /* Iterate over the options in the packet */
    GError *iteration_error = NULL;
    while (option_iterator_next(&iterator, &iteration_error)) {
      if (iterator.option_tag == tag) {  // This is the one we're looking for
	if (option_value == NULL)
	  option_value = g_byte_array_new();

	/* Build return value from option value(s) */
	g_byte_array_append(option_value,
			    iterator.value_bytes, iterator.value_length);
      }
    }

    if (iteration_error != NULL) {
      if (option_value != NULL) g_byte_array_free(option_value, TRUE);
      g_propagate_error(error, iteration_error);
      return NULL;
    }
  }

  return option_value;
}

/* packet_get_and_convert_option_impl()
 *
 * Retrieves the value of an option from the packet and converts it to a POD
 * type.  This function ties together packet_get_option() and bytes_to_var()
 * for convenience.
 *
 * This function should typically be invoked via the
 * packet_get_and_convert_option() macro, which allows the caller to specify
 * just the name of the target variable rather than its address and size.
 */
gboolean packet_get_and_convert_option_impl(DhcpPacket const *packet, guint8 tag,
					    void *var_ptr, gsize var_size, GError **error) {
  g_return_val_if_fail(packet != NULL, FALSE);
  g_return_val_if_fail(var_ptr != NULL && var_size > 0, FALSE);
  g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

  GError *get_option_error = NULL;
  GByteArray *option_value = packet_get_option(packet, tag, &get_option_error);

  if (option_value) {
    GError *bytes_to_var_error = NULL;
    bytes_to_var_impl(option_value, var_ptr, var_size, &bytes_to_var_error);
    g_byte_array_free(option_value, TRUE);

    if (bytes_to_var_error != NULL) {
      if (g_error_matches(bytes_to_var_error,
			  DHCPMASQD_BYTES_TO_VAR_ERROR,
			  DHCPMASQD_BYTES_TO_VAR_ERROR_WRONG_SIZE)) {
	g_propagate_prefixed_error(error, bytes_to_var_error,
				   "wrong size option %u: ", tag);
      }
      else {
	g_propagate_error(error, bytes_to_var_error);
      }

      return FALSE;
    }
    else return TRUE;  // Successfully converted option
  }
  else if (get_option_error != NULL) {
    g_propagate_error(error, get_option_error);
    return FALSE;
  }
  else return FALSE;  // Requested option not present in packet
}

/* packet_remove_option()
 *
 * Removes all occurrences of an option from the packet.
 *
 * If an error is encountered while processing the packet's options,
 * it will be reported via the GError parameter.  The packet may have been
 * partially modified when this occurs.
 */
void packet_remove_option(DhcpPacket const *packet, guint8 tag, GError **error) {
  g_return_if_fail(packet != NULL);
  g_return_if_fail(error == NULL || *error == NULL);

  for (guint i = 0; i < NUM_OPTION_AREAS; i += 1) {
    struct option_area const *option_area = &(packet->option_areas[i]);
    struct option_iterator iterator;
    option_iterator_init(&iterator, option_area);

    guint8 *move_to = NULL;  // For moving options downward to fill gaps
    gsize total_removed_length = 0;  // Bytes worth of options removed

    /* Iterate over the options in the packet */
    GError *iteration_error = NULL;
    while (option_iterator_next(&iterator, &iteration_error)) {
      if (iterator.option_tag == tag) {  // This is the one we're looking for
	/* From this point forward, options will need to be shifted down
	   to fill the gap left by this one */
	if (move_to == NULL) {
	  move_to = iterator.option_bytes;
	}

	/* All the gaps from removed options will "bubble" to the end of the
	   option area, forming a single gap of this size */
	total_removed_length += iterator.option_length;
      }
      else if (move_to != NULL) {
	/* This option should remain in the packet, but another option before
	   it has been removed, so move it down to fill the gap */
	g_memmove(move_to, iterator.option_bytes, iterator.option_length);
	move_to += iterator.option_length;  // gap has moved upward
      }
    }

    /* If options were moved down to fill gaps left by removals, the space that
       they used to occupy needs to be cleared */
    if (move_to != NULL) {
      memset(move_to, DHO_PAD, total_removed_length);
    }

    if (iteration_error != NULL) {
      g_propagate_error(error, iteration_error);
      return;
    }
  }
}

/* packet_add_option()
 *
 * Adds an option to the packet.  This should be used only if the option is
 * not already present in the packet; if it is, remove the old value first.
 * (If the option is already present and split across multiple option areas,
 *  the new value may end up being interleaved with the old.)
 *
 * The split_allowed parameter controls whether RFC 3396 option splitting is
 * permitted.  If splitting is necessary but not permitted, the GError parameter
 * will be set with a code of DHCPMASQD_PACKET_ERROR_SPLIT_REQUIRED.  The packet
 * may have already been partially modified when this occurs.
 *
 * If the option's body is to be empty, NULL may be passed for the value,
 * as an alternative to passing a GByteArray of length zero.
 *
 * Only variable-length options are permitted.  The two fixed-length options
 * (DHO_PAD and DHO_END) are forbidden.
 *
 * If an error is encountered while processing the packet's options, it will be
 * reported via the GError parameter.  The packet may have already been
 * partially modified when this occurs.
 */
void packet_add_option(DhcpPacket const *packet, guint8 tag,
		       GByteArray const *value, gboolean split_allowed,
		       GError **error) {
  g_return_if_fail(packet != NULL);
  g_return_if_fail((tag != DHO_PAD) && (tag != DHO_END));
  g_return_if_fail(error == NULL || *error == NULL);

  guint8 const *value_bytes = (value == NULL) ? NULL : value->data;
  guint value_length = (value == NULL) ? 0 : value->len;

  if (value_length > G_MAXUINT8 && !split_allowed) {
    g_set_error(error,
		DHCPMASQD_PACKET_ERROR,
		DHCPMASQD_PACKET_ERROR_SPLIT_REQUIRED,
		"Option value is %u bytes long, "
		"but all values longer than %" G_GUINT16_FORMAT " bytes "
		"require splitting", value_length, ((guint16) G_MAXUINT8));
    return;
  }

  for (guint i = 0; i < NUM_OPTION_AREAS; i += 1) {
    struct option_area const *option_area = &(packet->option_areas[i]);
    struct option_iterator iterator;
    option_iterator_init(&iterator, option_area);

    /* Iterate over the options in the packet */
    GError *iteration_error = NULL;
    while (option_iterator_next(&iterator, &iteration_error)) {
      if (iterator.option_tag == DHO_END) {
	/* The space after the DHO_END is free space, available for writing
	   more options (and relocating the DHO_END accordingly) */
	guint8 *free_space_bytes =
	  iterator.option_bytes + iterator.option_length;

	while (free_space_bytes < iterator.option_area_end) {
	  /* At this point we have a nonzero amount of space available */
	  gsize const free_space_length =
	    iterator.option_area_end - free_space_bytes;

	  /* Determine how many bytes should go into the next option chunk */
	  /* (Clipping to G_MAXUINT8 is OK because if it's necessary, we
	      already verified earlier in this function that it's permitted) */
	  gsize chunk_value_length = value_length;
	  if (chunk_value_length > G_MAXUINT8)
	    chunk_value_length = G_MAXUINT8;

	  /* If there's not enough room for this chunk's value plus its tag and
	     length bytes, it'll have to be split so the rest can go into the
	     next option area */
	  gsize chunk_option_length = chunk_value_length + 2;
	  if (free_space_length < chunk_option_length) {
	    if (!split_allowed) {
	      g_set_error(error,
			  DHCPMASQD_PACKET_ERROR,
			  DHCPMASQD_PACKET_ERROR_SPLIT_REQUIRED,
			  "Need %" G_GSIZE_FORMAT " bytes for next option "
			  "chunk but only %" G_GSIZE_FORMAT " are available",
			  chunk_option_length, free_space_length);
	      return;
	    }

	    chunk_option_length = free_space_length;
	    chunk_value_length = chunk_option_length - 2;
	  }

	  guint8 *chunk_option_bytes = iterator.option_bytes;
	  guint8 *chunk_value_bytes = chunk_option_bytes + 2;

	  /* Move the DHO_END option back to make room for the new option
	     before it */
	  iterator.option_bytes += chunk_option_length;
	  free_space_bytes += chunk_option_length;
	  g_memmove(iterator.option_bytes, chunk_option_bytes,
		    iterator.option_length);

	  /* Construct the new option chunk */
	  chunk_option_bytes[0] = tag;
	  chunk_option_bytes[1] = (guint8) chunk_value_length;
	  if (chunk_value_length > 0)
	    memcpy(chunk_value_bytes, value_bytes, chunk_value_length);
	  value_bytes += chunk_value_length;
	  value_length -= chunk_value_length;

	  /* If there's nothing needing to go in a subsequent option chunk,
	     we're done */
	  if (value_length == 0) return;
	}

	/* The iterator should still be valid at this point since it's been
	   adjusted for the new position of the DHO_END option, and the loop
	   will terminate now since there's nothing after DHO_END. */
      }
    }

    if (iteration_error != NULL) {
      g_propagate_error(error, iteration_error);
      return;
    }
  }

  /* If not all of the new option's value has been put into the packet after
     going through all the option areas, the option is just too big for this
     packet */
  if (value_length > 0) {
    g_set_error(error,
		DHCPMASQD_PACKET_ERROR,
		DHCPMASQD_PACKET_ERROR_INSUFFICIENT_SPACE,
		"Option value was too big for packet: %u bytes left",
		value_length);
    return;
  }
}

/*
 * packet_get_effective_client_id()
 *
 * Determines a packet's effective client identifier.  This is its actual
 * client identifier if one is present in the packet, otherwise its hardware
 * address, as specified in RFC 2131.
 *
 * The caller is responsible for freeing the returned GByteArray.
 *
 * If an error is encountered while processing the packet's options,
 * it will be reported via the GError parameter, and NULL will be returned.
 */
GByteArray *packet_get_effective_client_id(DhcpPacket const *packet,
					   GError **error) {
  g_return_val_if_fail(packet != NULL, NULL);
  g_return_val_if_fail(error == NULL || *error == NULL, NULL);

  /* Get the packet's client ID, if it has one */
  GError *get_client_id_option_error = NULL;
  GByteArray *client_id =
    packet_get_option(packet, DHO_DHCP_CLIENT_IDENTIFIER,
		      &get_client_id_option_error);

  if (get_client_id_option_error != NULL) {
    g_propagate_error(error, get_client_id_option_error);
    return NULL;
  }

  /* If no client ID given, use its hardware address */
  if (client_id == NULL) {
    client_id = packet_get_client_hwaddr(packet);
  }

  return client_id;
}

GByteArray const *packet_prepare_for_transmit(DhcpPacket *packet) {
  g_return_val_if_fail(packet != NULL, NULL);

  /* The checksum fields in the packet should already be zeroed, so we can
     just go ahead and recalculate the checksums */
  packet->hdr_udp->check =
    g_htons(udp_checksum(packet->hdr_udp,
			 packet->hdr_ip->saddr,
			 packet->hdr_ip->daddr));
  packet->hdr_ip->check = g_htons(ip_checksum(packet->hdr_ip));

  return packet->raw_frame;
}

/* ------------------------------------------------------------------------- */

/* init_and_verify()
 *
 * Checks whether a received DHCP packet is valid, and initializes the fields
 * of the DhcpPacket structure.
 *
 * The IP and UDP headers checksums are verified, to defend against corrupted
 * packets.  The DHCP "magic cookie" is checked to confirm that the packet is
 * really a DHCP packet.
 *
 * Returns TRUE if all checks passed, otherwise FALSE.
 */
gboolean init_and_verify(DhcpPacket *packet, GError **error) {
  /* Find Ethernet header */
  packet->hdr_ethernet = (struct ether_header *) packet->raw_frame->data;

  /* Find IP header */
  packet->hdr_ip = SKIP_ETHERNET_HEADER(packet->hdr_ethernet);
  if (G_UNLIKELY((gconstpointer) packet->hdr_ip >=
		 (gconstpointer) packet->payload_end)) {
    /* Packet ends before IP header begins */
    g_set_error(error, DHCPMASQD_PACKET_ERROR, DHCPMASQD_PACKET_ERROR_MALFORMED,
		"truncated before IP header");
    return FALSE;
  }

  /* Verify that we have at least the fixed portion of the IP header
     (which contains its length field) */
  if (G_UNLIKELY((gconstpointer) BYTE_OFFSET(packet->hdr_ip, sizeof(*(packet->hdr_ip))) >
		 (gconstpointer) packet->payload_end)) {
    /* Packet contains an incomplete IP header */
    g_set_error(error, DHCPMASQD_PACKET_ERROR, DHCPMASQD_PACKET_ERROR_MALFORMED,
		"truncated within IP header");
    return FALSE;
  }

  /* Verify IP header length fields */
  guint16 ip_length = ntohs(packet->hdr_ip->tot_len);
  gsize ip_length_received =
    ((gpointer) packet->payload_end) - ((gpointer) packet->hdr_ip);
  if (G_UNLIKELY(ip_length != ip_length_received)) {
    /* Stated length and actual length don't match */
    g_set_error(error, DHCPMASQD_PACKET_ERROR, DHCPMASQD_PACKET_ERROR_MALFORMED,
		"incorrect IP length field");
    return FALSE;
  }
  if (G_UNLIKELY(IP_HDR_BYTES(packet->hdr_ip->ihl) < sizeof(struct iphdr))) {
    /* Stated header length is less than the fixed minimum size of an IP header
       with no options */
    g_set_error(error, DHCPMASQD_PACKET_ERROR, DHCPMASQD_PACKET_ERROR_MALFORMED,
		"incorrect IP header length field");
    return FALSE;
  }
  if (G_UNLIKELY(IP_HDR_BYTES(packet->hdr_ip->ihl) > ip_length)) {
    /* Header is larger than the entire packet */
    g_set_error(error, DHCPMASQD_PACKET_ERROR, DHCPMASQD_PACKET_ERROR_MALFORMED,
		"incorrect IP header length field");
    return FALSE;
  }

  /* Verify IP header checksum */
  guint16 const ip_check = ntohs(packet->hdr_ip->check);
  packet->hdr_ip->check = 0;
  if (G_UNLIKELY(ip_checksum(packet->hdr_ip) != ip_check)) {
    g_set_error(error, DHCPMASQD_PACKET_ERROR, DHCPMASQD_PACKET_ERROR_MALFORMED,
		"incorrect IP checksum");
    return FALSE;
  }

  /* Verify that this is a UDP packet */
  if (G_UNLIKELY(packet->hdr_ip->protocol != IPPROTO_UDP)) {
    g_set_error(error, DHCPMASQD_PACKET_ERROR, DHCPMASQD_PACKET_ERROR_MALFORMED,
		"transport layer protocol is not UDP");
    return FALSE;
  }

  /* Find UDP header */
  packet->hdr_udp = SKIP_IP_HEADER(packet->hdr_ip);
  if (G_UNLIKELY((gconstpointer) packet->hdr_udp >=
		 (gconstpointer) packet->payload_end)) {
    /* Packet ends before UDP header begins */
    g_set_error(error, DHCPMASQD_PACKET_ERROR, DHCPMASQD_PACKET_ERROR_MALFORMED,
		"truncated before UDP header");
    return FALSE;
  }

  /* Verify that we have a complete UDP header */
  if (G_UNLIKELY((gconstpointer) BYTE_OFFSET(packet->hdr_udp, sizeof(*(packet->hdr_udp))) >
		 (gconstpointer) packet->payload_end)) {
    /* Packet contains an incomplete UDP header */
    g_set_error(error, DHCPMASQD_PACKET_ERROR, DHCPMASQD_PACKET_ERROR_MALFORMED,
		"truncated within UDP header");
    return FALSE;
  }

  /* Verify UDP header length field */
  guint16 udp_length = ntohs(packet->hdr_udp->len);
  gsize udp_length_received =
    ((gpointer) packet->payload_end) - ((gpointer) packet->hdr_udp);
  if (G_UNLIKELY(udp_length != udp_length_received)) {
    /* Stated length and actual length don't match */
    g_set_error(error, DHCPMASQD_PACKET_ERROR, DHCPMASQD_PACKET_ERROR_MALFORMED,
		"incorrect UDP length field");
    return FALSE;
  }

  /* Verify UDP header checksum */
  guint16 const udp_check = ntohs(packet->hdr_udp->check);
  packet->hdr_udp->check = 0;
  if (G_UNLIKELY(udp_checksum(packet->hdr_udp, packet->hdr_ip->saddr, packet->hdr_ip->daddr) != udp_check)) {
    g_set_error(error, DHCPMASQD_PACKET_ERROR, DHCPMASQD_PACKET_ERROR_MALFORMED,
		"incorrect UDP checksum");
    return FALSE;
  }

  /* Find DHCP payload */
  packet->payload = SKIP_UDP_HEADER(packet->hdr_udp);
  if (G_UNLIKELY((gconstpointer) packet->payload >=
		 (gconstpointer) packet->payload_end)) {
    /* Packet ends before DHCP payload begins */
    g_set_error(error, DHCPMASQD_PACKET_ERROR, DHCPMASQD_PACKET_ERROR_MALFORMED,
		"truncated before payload");
    return FALSE;
  }

  /* Verify that we have a complete DHCP payload, including at least the
     "magic cookie" at the beginning of the options area */
  gsize const cookie_length = strlen(DHCP_OPTIONS_COOKIE);
  if (G_UNLIKELY((gconstpointer) packet->payload->options + cookie_length >
		 (gconstpointer) packet->payload_end)) {
    /* Packet contains an incomplete DHCP message */
    g_set_error(error, DHCPMASQD_PACKET_ERROR, DHCPMASQD_PACKET_ERROR_MALFORMED,
		"truncated within payload");
    return FALSE;
  }

  /* Verify DHCP magic cookie */
  if (G_UNLIKELY(memcmp(packet->payload->options, DHCP_OPTIONS_COOKIE,
		      strlen(DHCP_OPTIONS_COOKIE)) != 0)) {
    g_set_error(error, DHCPMASQD_PACKET_ERROR, DHCPMASQD_PACKET_ERROR_MALFORMED,
		"invalid magic cookie");
    return FALSE;
  }

  /* Set up access to the packet's main options area */
  /* (This one must always be present.) */
  packet->option_areas[OPTIONS_MAIN].bytes = packet->payload->options + cookie_length;
  packet->option_areas[OPTIONS_MAIN].length = packet->payload_end - packet->option_areas[OPTIONS_MAIN].bytes;
  g_assert(packet->payload_end >= packet->option_areas[OPTIONS_MAIN].bytes);

  /* Initialize the other two option areas as unused */
  /* (This is so we can safely use packet_get_option() below) */
  packet->option_areas[OPTIONS_IN_FILE].bytes = NULL;
  packet->option_areas[OPTIONS_IN_FILE].length = 0;
  packet->option_areas[OPTIONS_IN_SNAME].bytes = NULL;
  packet->option_areas[OPTIONS_IN_SNAME].length = 0;

  gboolean packet_has_options_in_file = FALSE;
  gboolean packet_has_options_in_sname = FALSE;

  /* Check whether this packet is using option overload */
  guint8 overload;
  GError *get_overload_option_error = NULL;
  if (packet_get_and_convert_option(packet, DHO_DHCP_OPTION_OVERLOAD,
				    overload, &get_overload_option_error)) {
    /* This could be treated as a bit mask, but a switch with distinct values
       corresponds more closely to how it's defined in RFC 2132 */
    switch (overload) {
    case 0:
      /* Nothing to do */
      break;

    case 1:
      packet_has_options_in_file = TRUE;
      break;

    case 2:
      packet_has_options_in_sname = TRUE;
      break;

    case 3:
      packet_has_options_in_file = TRUE;
      packet_has_options_in_sname = TRUE;
      break;

    default:
      g_set_error(error,
		  DHCPMASQD_PACKET_ERROR, DHCPMASQD_PACKET_ERROR_MALFORMED,
		  "invalid overload option value: %u", (guint) overload);
      return FALSE;
    }
  }
  else if (get_overload_option_error != NULL) {
    g_propagate_error(error, get_overload_option_error);
    return FALSE;
  }

  /* Set up access to options in the packet's "file" field, if applicable */
  if (packet_has_options_in_file) {
    guint8 *option_area_bytes = (guint8 *) &(packet->payload->file);
    gsize option_area_length = sizeof(packet->payload->file);

    packet->option_areas[OPTIONS_IN_FILE].bytes = option_area_bytes;
    packet->option_areas[OPTIONS_IN_FILE].length = option_area_length;
  }
  else {
    packet->option_areas[OPTIONS_IN_FILE].bytes = NULL;
    packet->option_areas[OPTIONS_IN_FILE].length = 0;
  }

  /* Set up access to options in the packet's "sname" field, if applicable */
  if (packet_has_options_in_sname) {
    guint8 *option_area_bytes = (guint8 *) &(packet->payload->sname);
    gsize option_area_length = sizeof(packet->payload->sname);

    packet->option_areas[OPTIONS_IN_SNAME].bytes = option_area_bytes;
    packet->option_areas[OPTIONS_IN_SNAME].length = option_area_length;
  }
  else {
    packet->option_areas[OPTIONS_IN_SNAME].bytes = NULL;
    packet->option_areas[OPTIONS_IN_SNAME].length = 0;
  }

  /* Get the packet's DHCP message type */
  /* (Though an "option", every DHCP packet is required to have this) */
  guint8 message_type;
  GError *get_message_type_option_error = NULL;
  if (packet_get_and_convert_option(packet, DHO_DHCP_MESSAGE_TYPE,
				    message_type, &get_message_type_option_error)) {
    packet->message_type = message_type;
  }
  else if (get_message_type_option_error != NULL) {
    g_propagate_prefixed_error(error, get_message_type_option_error,
			       "failed to retrieve message-type option: ");
    return FALSE;
  }
  else {
    g_set_error(error,
		DHCPMASQD_PACKET_ERROR, DHCPMASQD_PACKET_ERROR_OPTION_MISSING,
		"no message-type option");
    return FALSE;
  }

  /* All checks passed; we have a valid DHCP packet. */
  return TRUE;
}

/* option_iterator_init()
 *
 * Initializes a struct option_iterator for iterating over the options in
 * an option area of a packet.  After calling this, use option_iterator_next()
 * in a loop.
 */
static void option_iterator_init(struct option_iterator *iterator,
				 struct option_area const *option_area) {
  g_return_if_fail(iterator != NULL);
  iterator->next_option = NULL;  // fail-safe if option_area is null
  g_return_if_fail(option_area != NULL);

  if (option_area->bytes && option_area->length) {
    /* Prepare for first call to option_iterator_next() */
    iterator->next_option = option_area->bytes;
    iterator->option_area_end = option_area->bytes + option_area->length;
  }
  else {  // option_area is unused (bytes == NULL) or empty (length == 0)
    /* Arrange for option_iterator_next() to return FALSE immediately */
    iterator->next_option = NULL;
  }
}

/* option_iterator_next()
 *
 * Iterates over the options in an option area of a packet.
 *
 * Returns TRUE if the iterator has been updated with information about an
 * option, or FALSE if there are no more options.
 *
 * If an error is encountered while processing the options, it will be reported
 * via the GError parameter, and FALSE will be returned.
 */
static gboolean option_iterator_next(struct option_iterator *iterator,
				     GError **error) {
  g_return_val_if_fail(iterator != NULL, FALSE);

  if (iterator->next_option == NULL) {
    return FALSE;  // No more options left
  }

  iterator->option_bytes = iterator->next_option;
  iterator->option_tag = iterator->option_bytes[0];
  if (iterator->option_tag == DHO_PAD) {  // Fixed-length option
    iterator->option_length = 1;
    iterator->value_bytes = NULL;
    iterator->value_length = 0;
    iterator->next_option = iterator->option_bytes + 1;
  }
  else if (iterator->option_tag == DHO_END) {  // Fixed-length option
    iterator->option_length = 1;
    iterator->value_bytes = NULL;
    iterator->value_length = 0;
    iterator->next_option = NULL;  // By definition, this is the last option
  }
  else {  // Variable-length option
    /* This option's tag byte should not be the last byte in the option area */
    if (G_UNLIKELY((iterator->option_bytes + 1) >= iterator->option_area_end)) {
      g_set_error(error,
		  DHCPMASQD_PACKET_ERROR,
		  DHCPMASQD_PACKET_ERROR_MALFORMED,
		  "Option truncated before length field");
      return FALSE;
    }

    iterator->value_bytes = iterator->option_bytes + 2;  // Skip tag and length
    iterator->value_length = iterator->option_bytes[1];
    iterator->option_length = iterator->value_length + 2;
    iterator->next_option = iterator->option_bytes + iterator->option_length;

    /* The option area should not end in the middle of this option's body */
    if (G_UNLIKELY(iterator->next_option > iterator->option_area_end)) {
      g_set_error(error,
		  DHCPMASQD_PACKET_ERROR,
		  DHCPMASQD_PACKET_ERROR_MALFORMED,
		  "Option truncated within body");
      return FALSE;
    }
  }

  /* This will be true for the last option in the area if there's no DHO_END */
  if (iterator->next_option >= iterator->option_area_end) {
    iterator->next_option = NULL;
  }

  return TRUE;  // option_iterator structure filled out successfully
}

/* ip_checksum()
 *
 * Calculates the checksum of an IP packet header
 */
guint16 ip_checksum(struct iphdr const *hdr) {
  /* Checksum over the given header length */
  return checksum_final(checksum_core((guint16 const *)hdr,
				      IP_HDR_WORDS(hdr->ihl)));
}

/* udp_checksum()
 *
 * Calculates the checksum of a UDP packet header
 *
 * IP source and destination addresses should be in network byte order
 */
guint16 udp_checksum(struct udphdr const *hdr,
		     guint32 ip_saddr, guint32 ip_daddr) {
  /* UDP "pseudo header" */
  union {
    struct {
      guint32 ups_saddr;
      guint32 ups_daddr;
      guint16 ups_proto;
      guint16 ups_length;
    } fields;

    guint16 words[6];
  } pseudo;

  pseudo.fields.ups_saddr = ip_saddr;
  pseudo.fields.ups_daddr = ip_daddr;
  pseudo.fields.ups_proto = g_htons(IPPROTO_UDP);
  pseudo.fields.ups_length = hdr->len;
  guint16 length = g_ntohs(pseudo.fields.ups_length);

  /* Calculate checksum of pseudo header first */
  guint32 sum =
    checksum_core(pseudo.words, sizeof(pseudo.words) / sizeof(*pseudo.words));

  /* Continue checksumming with the actual packet contents */
  sum += checksum_core((guint16 const *) hdr, length >> 1);

  /* If the packet length is odd, add in the last byte and an imaginary zero
     byte following it */
  if (length & 1) {  // Odd number?
    guint16 tail =
      g_ntohs((guint16) (*((guint8 const *) hdr + (length - 1))) << 8);
    sum += checksum_core(&tail, sizeof(tail) >> 1);
  }

  /* Do the final negation and return */
  return checksum_final(sum);
}

/* checksum_core()
 *
 * The "core" of the checksum routines - adds 16-bit words
 */
guint32 checksum_core(guint16 const *data, unsigned int count) {
  guint32 sum = 0;

  /* Add up 16-bit words */
  for (guint i = 0; i < count; ++i)
    sum += g_ntohs(data[i]);

  return sum;
}

/* checksum_final()
 *
 * "Finalizes" an intermediate checksum produced by checksum_core() by
 * adding the carries back into the 16-bit value to obtain the 1's-complement
 * sum, and performing the 1's-complement negation
 */
guint16 checksum_final(guint32 const presum) {
  return ~((guint16) presum + (guint16) (presum >> 16));
}
