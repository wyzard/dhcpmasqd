/* interfaces.h - interface for interfaces.c
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTERFACES_H
#define INTERFACES_H

#include <glib.h>

G_BEGIN_DECLS

typedef struct _Interface Interface;

void interfaces_init(gchar const *ifname_internal,
		     gchar const *ifname_external);
void interfaces_cleanup();

void interface_transmit_frame(Interface const *interface,
			      GByteArray const *raw_frame);

G_END_DECLS

#endif  /* INTERFACES_H */
