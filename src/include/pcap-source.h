/* pcap_source.h -  interface for pcap-source.c
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PCAP_SOURCE_H
#define PCAP_SOURCE_H

#include <glib.h>
#include <pcap.h>

G_BEGIN_DECLS

/* The type of the callback function used for handling captured packets. */
typedef gboolean (*PcapSourceFunc)(GTimeVal const *now,
				   const struct pcap_pkthdr *,
				   const guchar *,
				   gpointer);

GSource *pcap_source_new(pcap_t *pcap);
guint pcap_source_add(pcap_t *pcap, PcapSourceFunc callback, gpointer user_data);
guint pcap_source_add_full(pcap_t *pcap, PcapSourceFunc callback, gpointer user_data, GDestroyNotify notify);

G_END_DECLS

#endif  /* PCAP_SOURCE_H */
