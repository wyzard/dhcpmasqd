/* logging.h - interface for logging.c
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOGGING_H
#define LOGGING_H

#include <glib.h>
#include <syslog.h>  /* For the priority codes */

void log_perror(gint priority, gchar const *prefix);
void log_msg(gint priority, gchar const *format, ...) G_GNUC_PRINTF (2, 3);
void log_fatal_perror(gint priority, gchar const *prefix, gint status) G_GNUC_NORETURN;
void log_fatal_msg(gint priority, gchar const *format, gint status, ...) G_GNUC_NORETURN G_GNUC_PRINTF (2, 4);

#endif  /* LOGGING_H */
