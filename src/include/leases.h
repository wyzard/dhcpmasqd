/* leases.h - interface for leases.c
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LEASES_H
#define LEASES_H

#include <glib.h>

G_BEGIN_DECLS

typedef struct _DhcpLease DhcpLease;

void leases_init();
void leases_cleanup();

DhcpLease *lease_get_by_ipaddr(guint32 client_ipaddr);
DhcpLease *lease_begin(guint32 client_ipaddr, GByteArray *client_id,
		       GTimeVal const *expiry);
void lease_renew(DhcpLease *lease, GTimeVal const *expiry);
void lease_expire(DhcpLease *lease);
void lease_terminate(DhcpLease *lease);

GByteArray const *lease_get_client_id(DhcpLease const *lease);
GTimeVal const *lease_expiry(DhcpLease const *lease);

/* Trigger a refresh of the on-disk lease database, which is managed internally
   by the module */
void refresh_lease_database();

G_END_DECLS

#endif  /* LEASES_H */
