/* event-queue.h - interface for event-queue.c
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EVENT_QUEUE_H
#define EVENT_QUEUE_H

#include <glib.h>

G_BEGIN_DECLS

typedef struct _EventQueue EventQueue;

/* Create an event queue */
EventQueue *event_queue_create();

/* Destroy an event queue */
typedef void (*EventCleanupFunc) (gpointer data, gpointer user_data);
void event_queue_destroy(EventQueue *event_queue,
			 EventCleanupFunc cleanup_func, gpointer user_data);

/* Add an event to the queue */
void event_queue_add_event(EventQueue *event_queue,
			   GTimeVal const *when, gpointer data);

/* Remove an event from the queue, matching on its data pointer */
void event_queue_remove_event(EventQueue *event_queue, gconstpointer data);

/* Get the time when the next event is scheduled to occur,
   or NULL if the queue is empty */
GTimeVal const *event_queue_next_event_time(EventQueue *event_queue);

/* Remove the oldest expired event from the queue and return its data
   pointer, or NULL if no events are expired */
gpointer event_queue_get_expired_event(EventQueue *event_queue,
				       GTimeVal const *now);

/* Tools for time deltas */
void timediff(GTimeVal *result, GTimeVal const *to, GTimeVal const *from);
gint timecmp(GTimeVal const *t1, GTimeVal const *t2);

G_END_DECLS

#endif  /* EVENT_QUEUE_H */
