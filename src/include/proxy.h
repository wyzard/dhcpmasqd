/* proxy.h - interface for proxy.c
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROXY_H
#define PROXY_H

#include <glib.h>

#include "interfaces.h"
#include "packets.h"

G_BEGIN_DECLS

void proxy_outward(DhcpPacket *packet,
		   Interface const *interface_from,
		   Interface const *interface_to,
		   GTimeVal const *now, GError **error);
void proxy_inward(DhcpPacket *packet,
		  Interface const *interface_from,
		  Interface const *interface_to,
		  GTimeVal const *now, GError **error);

G_END_DECLS

#endif  /* PROXY_H */
