/* signal_source.h - interface for signal-source.c
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGNAL_SOURCE_H
#define SIGNAL_SOURCE_H

#include <glib.h>

G_BEGIN_DECLS

/* The type of the callback function used for handling queued signals. */
typedef gboolean (*SignalSourceFunc)(gint signal, gpointer);

/* Specify a signal which should be queued by this source. */
gint signal_source_handle_signal(gint const signal);

GSource *signal_source_new();
guint signal_source_add(SignalSourceFunc callback, gpointer user_data);
guint signal_source_add_full(SignalSourceFunc callback, gpointer user_data, GDestroyNotify notify);

G_END_DECLS

#endif  /* SIGNAL_SOURCE_H */
