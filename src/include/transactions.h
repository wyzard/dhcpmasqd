/* transactions.h - interface for transactions.c
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRANSACTIONS_H
#define TRANSACTIONS_H

#include <glib.h>

G_BEGIN_DECLS

typedef struct _DhcpTransaction DhcpTransaction;

void transactions_init();
void transactions_cleanup();

DhcpTransaction *transaction_get_by_xid(guint32 xid);
DhcpTransaction *transaction_begin(guint32 xid,
				   GByteArray *client_hwaddr,
				   GByteArray *client_id,
				   gboolean broadcast_flag,
				   GTimeVal const *now);
void transaction_continue(DhcpTransaction *transaction, GTimeVal const *now);

GByteArray const *transaction_client_hwaddr(DhcpTransaction const *transaction);
GByteArray const *transaction_get_client_id(DhcpTransaction const *transaction);
gboolean transaction_get_broadcast_flag(DhcpTransaction const *transaction);

G_END_DECLS

#endif  /* TRANSACTIONS_H */
