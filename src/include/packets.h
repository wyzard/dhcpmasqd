/* packets.h - interface for packets.c
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PACKETS_H
#define PACKETS_H

#include <glib.h>
#include <libnet.h>

#include "transactions.h"

G_BEGIN_DECLS

typedef struct _DhcpPacket DhcpPacket;

GQuark dhcpmasqd_packet_error_quark();
#define DHCPMASQD_PACKET_ERROR dhcpmasqd_packet_error_quark()

typedef enum {
  DHCPMASQD_PACKET_ERROR_OPTION_MISSING,  // An option that was sought was not present
  DHCPMASQD_PACKET_ERROR_SPLIT_REQUIRED,  // Failed to add an option because splitting is needed but wasn't permitted
  DHCPMASQD_PACKET_ERROR_INSUFFICIENT_SPACE,  // No room for adding an option
  DHCPMASQD_PACKET_ERROR_MALFORMED,  // Malformed packet
} DhcpmasqdPacketError;

/* Creates a packet structure from a raw Ethernet frame */
DhcpPacket *packet_create(GByteArray *raw_frame, GError **error);

/* Release resources associated with a packet that's no longer used */
void packet_destroy(DhcpPacket *packet);

/* Functions for retrieving attributes of a DHCP packet */
guint32 packet_get_xid(DhcpPacket const *packet);  // Transaction ID
guint8  packet_get_message_type(DhcpPacket const *packet);  // Message type (DHCPDISCOVER, etc.)
guint32 packet_get_lease_addr(DhcpPacket const *packet);  // IP address granted by lease
guint32 packet_get_client_addr(DhcpPacket const *packet);  // IP address of already-configured client
GByteArray *packet_get_client_hwaddr(DhcpPacket const *packet);  // Client's hardware address
gboolean packet_get_broadcast_flag(DhcpPacket const *packet);  // Does client request that responses be broadcast?
void packet_set_broadcast_flag(DhcpPacket *packet, gboolean broadcast);  // Specify whether the server should broadcast its responses

/* Functions for working with DHCP options */
gboolean packet_has_split_options(DhcpPacket const *packet, GError **error);
GByteArray *packet_get_option(DhcpPacket const *packet, guint8 tag, GError **error);
#define packet_get_and_convert_option(packet, tag, var, error) \
  packet_get_and_convert_option_impl(packet, tag, &var, sizeof(var), error)
gboolean packet_get_and_convert_option_impl(DhcpPacket const *packet, guint8 tag,
					    void *var_ptr, gsize var_size, GError **error);
void packet_remove_option(DhcpPacket const *packet, guint8 tag, GError **error);
void packet_add_option(DhcpPacket const *packet, guint8 tag,
		       GByteArray const *value, gboolean split_allowed,
		       GError **error);

/* Higher-level utility functions */
GByteArray *packet_get_effective_client_id(DhcpPacket const *packet, GError **error);

/* Update checksums and return a pointer to the raw frame data */
GByteArray const *packet_prepare_for_transmit(DhcpPacket *packet);

G_END_DECLS

#endif  /* PACKETS_H */
