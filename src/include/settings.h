/* settings.h - interface for settings.c
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include <glib.h>

G_BEGIN_DECLS

/* Global configuration variables (defined in settings.c) */
extern gchar const *leasedb_filename, *lease_script_filename;
extern gchar const *ifn_internal, *ifn_external;
extern int leasedb_refresh_interval;
extern int transaction_timeout;
extern gchar const *client_id_suffix;

/* Read the configuration file and set the above variables accordingly */
void load_settings(gchar const *conf_filename);

G_END_DECLS

#endif  /* SETTINGS_H */
