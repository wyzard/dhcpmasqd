/* util.h - interface for util.c
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTIL_H
#define UTIL_H

#include <glib.h>

G_BEGIN_DECLS

GQuark dhcpmasqd_hex_to_bytes_error_quark();
#define DHCPMASQD_HEX_TO_BYTES_ERROR dhcpmasqd_hex_to_bytes_error_quark()

typedef enum {
  DHCPMASQD_HEX_TO_BYTES_ERROR_ODD_LENGTH,
  DHCPMASQD_HEX_TO_BYTES_ERROR_INVALID_HEX,
} DhcpmasqdHexToBytesError;

GQuark dhcpmasqd_bytes_to_var_error_quark();
#define DHCPMASQD_BYTES_TO_VAR_ERROR dhcpmasqd_bytes_to_var_error_quark()

typedef enum {
  DHCPMASQD_BYTES_TO_VAR_ERROR_WRONG_SIZE,
} DhcpmasqdBytesToVarError;

/* Convert between byte arrays and hex strings */
GString *bytes_to_hex(GByteArray const *bytes);
GByteArray *hex_to_bytes(GString const *hex, GError **error);

/* Move data between variables and (potentially unaligned) byte arrays */
#define bytes_to_var(bytes, var, error) \
  bytes_to_var_impl(bytes, &var, sizeof(var), error)
void bytes_to_var_impl(GByteArray const *bytes, void *var_ptr, gsize var_size,
		       GError **error);

G_END_DECLS

#endif  /* UTIL_H */
