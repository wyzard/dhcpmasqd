/* event-source.h - interface for event-source.c
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EVENT_SOURCE_H
#define EVENT_SOURCE_H

#include <glib.h>

#include "event-queue.h"

G_BEGIN_DECLS

/* The type of the callback function used for handling expired events. */
typedef gboolean (*EventSourceFunc)(gpointer data, gpointer);

GSource *event_source_new(EventQueue *event_queue);
guint event_source_add(EventQueue *event_queue, EventSourceFunc callback, gpointer user_data);
guint event_source_add_full(EventQueue *event_queue, EventSourceFunc callback, gpointer user_data, GDestroyNotify notify);

G_END_DECLS

#endif  /* EVENT_SOURCE_H */
