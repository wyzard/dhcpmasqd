/* settings.c - subsystem for reading settings from the configuration file
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <confuse.h>
#include <glib.h>

#include "logging.h"
#include "settings.h"

/* Global configuration values */
gchar const *leasedb_filename;
gchar const *lease_script_filename = NULL;
gchar const *ifn_internal = NULL, *ifn_external = NULL;
int leasedb_refresh_interval = 6*(60*60);  // 6 hours
int transaction_timeout = 60;  // 1 minute
gchar const *client_id_suffix = NULL;

#define CFG_SETTING_INTERNAL_INTERFACE "internal-interface"
#define CFG_SETTING_EXTERNAL_INTERFACE "external-interface"
#define CFG_SETTING_LEASE_DATABASE "lease-database-file"
#define CFG_SETTING_LEASE_DATABASE_REFRESH "lease-database-refresh"
#define CFG_SETTING_LEASE_SCRIPT "lease-script"
#define CFG_SETTING_TRANSACTION_TIMEOUT "transaction-timeout"
#define CFG_SETTING_CLIENT_ID_SUFFIX "client-id-suffix"

void load_settings(gchar const *conf_filename) {
  g_return_if_fail(conf_filename != NULL);

  cfg_opt_t opts[] = {
    CFG_STR(CFG_SETTING_INTERNAL_INTERFACE, NULL, CFGF_NONE),
    CFG_STR(CFG_SETTING_EXTERNAL_INTERFACE, NULL, CFGF_NONE),
    CFG_STR(CFG_SETTING_LEASE_DATABASE, DHCPMASQD_LEASE_FILE, CFGF_NONE),
    CFG_INT(CFG_SETTING_LEASE_DATABASE_REFRESH, leasedb_refresh_interval, CFGF_NONE),
    CFG_STR(CFG_SETTING_LEASE_SCRIPT, NULL, CFGF_NONE),
    CFG_INT(CFG_SETTING_TRANSACTION_TIMEOUT, transaction_timeout, CFGF_NONE),
    CFG_STR(CFG_SETTING_CLIENT_ID_SUFFIX, NULL, CFGF_NONE),
    CFG_END()
  };

  cfg_t *cfg = cfg_init(opts, CFGF_NONE);
  int parse_result = cfg_parse(cfg, conf_filename);
  if (parse_result == CFG_FILE_ERROR) {
    log_fatal_msg(LOG_ERR, "Failed to open configuration file: %s", 1,
		  conf_filename);
  }
  else if (parse_result == CFG_PARSE_ERROR) {
    log_fatal_msg(LOG_ERR, "Parse error while reading configuration file.", 1);
  }

  ifn_internal = g_strdup(cfg_getstr(cfg, CFG_SETTING_INTERNAL_INTERFACE));
  ifn_external = g_strdup(cfg_getstr(cfg, CFG_SETTING_EXTERNAL_INTERFACE));
  leasedb_filename = g_strdup(cfg_getstr(cfg, CFG_SETTING_LEASE_DATABASE));
  leasedb_refresh_interval = cfg_getint(cfg, CFG_SETTING_LEASE_DATABASE_REFRESH);
  lease_script_filename = g_strdup(cfg_getstr(cfg, CFG_SETTING_LEASE_SCRIPT));
  transaction_timeout = cfg_getint(cfg, CFG_SETTING_TRANSACTION_TIMEOUT);
  client_id_suffix = g_strdup(cfg_getstr(cfg, CFG_SETTING_CLIENT_ID_SUFFIX));

  /* Enforce required settings */
  if (ifn_internal == NULL) {
    log_fatal_msg(LOG_ERR, "Internal interface name not specified in configuration file.", 1);
  }
  else if (ifn_external == NULL) {
    log_fatal_msg(LOG_ERR, "External interface name not specified in configuration file.", 1);
  }
  else if (lease_script_filename == NULL) {
    log_fatal_msg(LOG_ERR, "Lease script filename not specified in configuration file.", 1);
  }

  cfg_free(cfg);
}
