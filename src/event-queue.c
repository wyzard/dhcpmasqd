/* event-queue.c - queue of event objects and times when they occur
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <glib.h>

#include "event-queue.h"

/* An event */
typedef struct _Event Event;
struct _Event {
  GTimeVal when;
  gpointer data;  // The pointer that was passed to event_queue_add_event()
};

/* An event queue */
struct _EventQueue {
  GList *event_list;
};

EventQueue *event_queue_create() {
  EventQueue *event_queue = g_slice_new(EventQueue);
  event_queue->event_list = NULL;
  return event_queue;
}

/* Helper structure used by event_queue_destroy() and
   event_queue_destroy_helper(), to pass two values through a single gpointer */
struct event_queue_destroy_helper_param {
  EventCleanupFunc cleanup_func;
  gpointer user_data;
};

/* Helper for event_queue_destroy() */
static void event_queue_destroy_helper(gpointer data, gpointer user_data) {
  Event *event = data;
  struct event_queue_destroy_helper_param const *helper_param = user_data;

  helper_param->cleanup_func(event->data, helper_param->user_data);
  g_slice_free(Event, event);
}

void event_queue_destroy(EventQueue *event_queue,
			 EventCleanupFunc cleanup_func, gpointer user_data) {
  g_return_if_fail(event_queue != NULL);

  /* Call the cleanup function for each remaining event in the queue */
  if (cleanup_func != NULL) {
    /* This is a bit of a kluge:  the given cleanup_func expects to receive
       the event's "data" pointer as its first parameter, not the Event pointer
       itself, so a helper function is necessary to make it work with
       g_list_foreach().  But the helper function needs both the cleanup_func
       and the user_data, so we have to use a temporary structure to pass both
       values through a single parameter. */
    struct event_queue_destroy_helper_param helper_param;
    helper_param.cleanup_func = cleanup_func;
    helper_param.user_data = user_data;

    g_list_foreach(event_queue->event_list,
		   event_queue_destroy_helper, &helper_param);
  }

  /* Remove any remaining events from the queue */
  g_list_free(event_queue->event_list);

  /* Free the queue structure itself */
  g_slice_free(EventQueue, event_queue);
}

/* Helper for event_queue_add_event() */
static gint add_event_compare_func(gconstpointer a, gconstpointer b) {
  Event const *event1 = a;
  Event const *event2 = b;

  /* The desired sort order is earliest-to-latest, and this function needs to
     return a positive value if event1 is later in the sort order than event2,
     so the result of timecmp() can be used directly. */
  return timecmp(&event1->when, &event2->when);
}

/* event_queue_add_event()
 *
 * Adds a new event to the list, in sorted order
 *
 * If the event is expired already, this will not be noticed
 */
void event_queue_add_event(EventQueue *event_queue,
			   GTimeVal const *when, gpointer data) {
  g_return_if_fail(event_queue != NULL);
  g_return_if_fail(when != NULL);

  /* Construct the new event object */
  Event *event = g_slice_new(Event);
  event->when = *when;
  event->data = data;

  /* Insert this new event into the list,
     in sorted order from soonest to latest */
  event_queue->event_list =
    g_list_insert_sorted(event_queue->event_list, event,
			 add_event_compare_func);
}

/* Helper for event_queue_remove_event() */
gint event_queue_remove_event_helper(gconstpointer a, gconstpointer b) {
  Event const *event = a;
  gconstpointer data = b;

  return (event->data != data);  // Return zero on equality
}

/* event_queue_remove_event()
 *
 * Remove an event from the queue, matching it by the data pointer that was
 * given when it was added to the queue.
 */
void event_queue_remove_event(EventQueue *event_queue, gconstpointer data) {
  g_return_if_fail(event_queue != NULL);

  GList *event_link =
    g_list_find_custom(event_queue->event_list, data,
		       event_queue_remove_event_helper);
  g_return_if_fail(event_link != NULL);
  Event *event = event_link->data;

  /* Free the event itself */
  g_slice_free(Event, event);

  /* Remove its link from the list */
  event_queue->event_list =
    g_list_remove_link(event_queue->event_list, event_link);
  g_list_free(event_link);
}

/* event_queue_next_event_time()
 *
 * Get the time when the next event is scheduled to occur.  If the event queue
 * is empty, return NULL instead.
 */
GTimeVal const *event_queue_next_event_time(EventQueue *event_queue) {
  g_return_val_if_fail(event_queue != NULL, NULL);

  GList const *first_list_item = event_queue->event_list;
  if (first_list_item == NULL)
    return NULL;
  else {
    Event const *first_event = first_list_item->data;
    return &first_event->when;
  }
}

/* event_queue_get_expired_timeout()
 *
 * Remove the oldest expired event from the queue and return its data pointer.
 * If no events are expired, return NULL instead.
 */
gpointer event_queue_get_expired_event(EventQueue *event_queue,
				       GTimeVal const *now) {
  g_return_val_if_fail(event_queue != NULL, NULL);
  g_return_val_if_fail(now != NULL, NULL);

  if (event_queue->event_list == NULL)
    return NULL;  // If there are no events, there are no expired events
  else {
    GTimeVal const *next_event_time = event_queue_next_event_time(event_queue);
    if (timecmp(now, next_event_time) >= 0) {  // Next event is expired
      /* Since the list is sorted, the "next event" is the first event, so
	 remove the first event and return its "data" pointer. */
      GList *first_list_item = event_queue->event_list;
      Event *first_event = first_list_item->data;
      gpointer first_event_data = first_event->data;

      /* event_queue_next_event_time() returns the time of the first event */
      g_assert(next_event_time == &(first_event->when));

      /* Remove from list and free the event */
      event_queue->event_list =
	g_list_delete_link(event_queue->event_list, first_list_item);
      g_slice_free(Event, first_event);

      return first_event_data;
    }
    else  // Next event isn't expired, so none of them are
      return NULL;
  }
}

/* timediff()
 *
 * Subtracts one GTimeVal from another to find the length of an interval
 *
 * result - the GTimeVal to put the result into
 * from, to - the starting and ending points of the interval
 */
void timediff(GTimeVal *result, GTimeVal const *to, GTimeVal const *from) {
  g_return_if_fail(result != NULL && to != NULL && from != NULL);

  /* Perform basic subtraction */
  result->tv_sec  = to->tv_sec  - from->tv_sec;
  result->tv_usec = to->tv_usec - from->tv_usec;

  /* Borrow a second to provide microseconds if necessary */
  if (result->tv_usec < 0) {
    --result->tv_sec;
    result->tv_usec += 1000000;
  }
}

/* timecmp()
 *
 * Compares two GTimeVals and returns positive, zero, or negative in the same
 * way the "cmp" function does for strings
 */
gint timecmp(GTimeVal const *t1, GTimeVal const *t2) {
  g_return_val_if_fail(t1 != NULL && t2 != NULL, 0);

  if (t1->tv_sec  > t2->tv_sec)  return  1;
  if (t1->tv_sec  < t2->tv_sec)  return -1;
  if (t1->tv_usec > t2->tv_usec) return  1;
  if (t1->tv_usec < t2->tv_usec) return -1;
  return 0;
}
