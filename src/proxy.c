/* interfaces.c - subsystem for sending and receiving DHCP packets
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <glib.h>
#include <net/ethernet.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "proxy.h"
#include "dhcp.h"
#include "leases.h"
#include "logging.h"
#include "settings.h"
#include "transactions.h"

/* static functions */
static void append_client_id_suffix(DhcpPacket *packet, GError **error);

/* proxy_outward()
 *
 * Proxies a DHCP packet from the internal side to the external.
 */
void proxy_outward(DhcpPacket *packet,
		   Interface const *interface_from,
		   Interface const *interface_to,
		   GTimeVal const *now, GError **error) {
  g_return_if_fail(packet != NULL);
  g_return_if_fail(interface_from != NULL);
  g_return_if_fail(interface_to != NULL);
  g_return_if_fail(error == NULL || *error == NULL);

  /* All DHCP packets coming from the internal network are relevant to us,
     so it doesn't matter if it was part of an existing transaction or a
     new one. */
  guint32 const xid = packet_get_xid(packet);
  DhcpTransaction *transaction = transaction_get_by_xid(xid);
  if (transaction != NULL) {
    transaction_continue(transaction, now);
  }
  else {  // New transaction
    GByteArray *effective_client_id =
      packet_get_effective_client_id(packet, error);

    if (effective_client_id == NULL) {  // Error occurred
      return;  // Error information is in the caller's error param
    }

    GByteArray *client_hwaddr = packet_get_client_hwaddr(packet);
    gboolean broadcast_flag = packet_get_broadcast_flag(packet);

    transaction =
      transaction_begin(xid, client_hwaddr, effective_client_id,
			broadcast_flag, now);
  }

  /* Check for termination of an existing lease */
  if (packet_get_message_type(packet) == DHCPRELEASE) {
    DhcpLease *lease = lease_get_by_ipaddr(packet_get_client_addr(packet));
    if (lease != NULL)
      lease_terminate(lease);
    /* TODO: Log a warning if the lease wasn't a known one?  */
  }

  /* Append a suffix to the client ID (if one is configured) */
  GError *append_client_id_suffix_error = NULL;
  append_client_id_suffix(packet, &append_client_id_suffix_error);
  if (append_client_id_suffix_error != NULL) {
    g_propagate_error(error, append_client_id_suffix_error);
    return;
  }

  /* Request that responses be broadcast so that we can receive them,
     since the real client can't receive unicasts from the server */
  packet_set_broadcast_flag(packet, TRUE);

  /* Pass the packet along to the external side */
  GByteArray const *raw_frame = packet_prepare_for_transmit(packet);
  interface_transmit_frame(interface_to, raw_frame);
}

/* proxy_inward()
 *
 * Proxies a DHCP packet from the external side to the internal.
 */
void proxy_inward(DhcpPacket *packet,
		  Interface const *interface_from,
		  Interface const *interface_to,
		  GTimeVal const *now, GError **error) {
  g_return_if_fail(packet != NULL);
  g_return_if_fail(interface_from != NULL);
  g_return_if_fail(interface_to != NULL);
  g_return_if_fail(now != NULL);
  g_return_if_fail(error == NULL || *error == NULL);

  guint32 const xid = packet_get_xid(packet);
  DhcpTransaction *transaction = transaction_get_by_xid(xid);

  if (transaction != NULL) {  // Part of one of our clients' transactions
    transaction_continue(transaction, now);

    /* If this packet is a DHCPACK establishing a new lease, register the
       new lease */
    /* (It's only establishing a new lease if the "yiaddr" field is filled
       in - responses to DHCPINFORM leave it set to zero) */
    if (packet_get_message_type(packet) == DHCPACK &&
	packet_get_lease_addr(packet) != 0) {
      GTimeVal expiry = *now;

      guint32 lease_time;
      GError *get_lease_time_option_error = NULL;
      if (packet_get_and_convert_option(packet, DHO_DHCP_LEASE_TIME,
					lease_time, &get_lease_time_option_error)) {
	expiry.tv_sec += g_ntohl(lease_time);
      }
      else if (get_lease_time_option_error != NULL) {
	g_propagate_prefixed_error(error, get_lease_time_option_error,
				   "failed to determine lease duration: ");
	return;
      }
      else {
	g_set_error(error,
		    DHCPMASQD_PACKET_ERROR,
		    DHCPMASQD_PACKET_ERROR_OPTION_MISSING,
		    "DHCPACK with no lease-time option");
	return;
      }

      /* Make a copy of the transaction's client ID to be owned by the lease */
      GByteArray const *transaction_client_id =
	transaction_get_client_id(transaction);
      GByteArray *client_id =
	g_byte_array_sized_new(transaction_client_id->len);
      g_byte_array_append(client_id,
			  transaction_client_id->data,
			  transaction_client_id->len);

      guint32 const client_ipaddr = packet_get_lease_addr(packet);
      DhcpLease *lease = lease_get_by_ipaddr(client_ipaddr);
      if (lease != NULL) {  // Already an outstanding lease for this IP
	/* Compare new lease's client ID to that of the existing lease */
	GByteArray const *lease_client_id = lease_get_client_id(lease);
	if (G_LIKELY(client_id->len == lease_client_id->len &&
		     memcmp(client_id->data, lease_client_id->data,
			    client_id->len) == 0)) {
	  /* Same client, so renew the existing lease */
	  lease_renew(lease, &expiry);

	  /* The client ID wasn't used to create a lease in this case,
	     so free it */
	  if (client_id != NULL)
	    g_byte_array_free(client_id, TRUE);
	}
	else {
	  /* Different client, so terminate the old lease and replace it
	     with this new one */
	  lease_terminate(lease);
	  lease = lease_begin(client_ipaddr, client_id, &expiry);
	}
      }
      else  // No outstanding lease for this IP; create one
	lease = lease_begin(client_ipaddr, client_id, &expiry);
    }

    /* Pass the packet along to the internal side */
    GByteArray const *raw_frame = packet_prepare_for_transmit(packet);
    interface_transmit_frame(interface_to, raw_frame);
  }
}

/* ------------------------------------------------------------------------- */

void append_client_id_suffix(DhcpPacket *packet, GError **error) {
  if (client_id_suffix == NULL) return;  // Nothing to do in this case

  /* Get the ID to which the suffix will be appended */
  GByteArray *client_id =
    packet_get_effective_client_id(packet, error);

  if (client_id == NULL ) {  // Error occurred
    return;  // Error information is in the caller's error param
  }

  /* Append the suffix */
  g_byte_array_append(client_id, (guint8 const *) client_id_suffix,
		      strlen(client_id_suffix));

  /* Remove the old ID (if any) from the packet */
  GError *remove_client_id_error = NULL;
  packet_remove_option(packet, DHO_DHCP_CLIENT_IDENTIFIER,
		       &remove_client_id_error);

  if (remove_client_id_error != NULL) {
    g_byte_array_free(client_id, TRUE);
    g_propagate_prefixed_error(error, remove_client_id_error,
			       "failed to remove client ID from packet "
			       "prior to adding suffixed ID: ");
    return;
  }

  /* Assume RFC 3396 option splitting is OK iff the packet already has it */
  GError *check_packet_has_split_options_error = NULL;
  gboolean split_allowed =
    packet_has_split_options(packet, &check_packet_has_split_options_error);

  if (check_packet_has_split_options_error != NULL) {
    g_byte_array_free(client_id, TRUE);
    g_propagate_error(error, check_packet_has_split_options_error);
    return;
  }

  /* Add the new ID */
  GError *add_client_id_error = NULL;
  packet_add_option(packet, DHO_DHCP_CLIENT_IDENTIFIER, client_id,
		    split_allowed, &add_client_id_error);
  g_byte_array_free(client_id, TRUE);

  if (add_client_id_error != NULL) {
    g_propagate_prefixed_error(error, add_client_id_error,
			       "failed to add suffixed client ID to packet: ");
    return;
  }
}
