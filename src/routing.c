/* routing.c - subsystem for updating routing tables as leases change
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <glib.h>
#include <libdaemon/dexec.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "logging.h"
#include "routing.h"
#include "settings.h"

enum lease_script_mode { lease_add, lease_remove };

static void run_lease_script(enum lease_script_mode mode, guint32 ip);

/* begin_routing()
 *
 * Calls the lease script to set up routing an IP address to the internal
 * network.
 */
void begin_routing(guint32 ip) {
  run_lease_script(lease_add, ip);
}

/* end_routing()
 *
 * Calls the lease script to stop routing an IP to the internal network
 */
void end_routing(guint32 ip) {
  run_lease_script(lease_remove, ip);
}

/* run_lease_script()
 *
 * Calls the lease script with the given parameters, in a child process
 */
void run_lease_script(enum lease_script_mode mode, guint32 ip) {
  /* Mode strings to pass to the lease script - these indices correspond to
     the values of 'enum lease_script_modes' */
  static gchar const *lease_mode_strings[] = { "add", "remove" };
  gchar ip_string[(3*4)+3+1];  // Long enough to hold "000.000.000.000"

  /* Build a string for the IP address - we do this before forking so we don't
     have to copy pages afterward */
  sprintf(ip_string, "%u.%u.%u.%u",
	  (ip >> 24) & 0xff,
	  (ip >> 16) & 0xff,
	  (ip >>  8) & 0xff,
	  (ip)       & 0xff);

  pid_t const child_pid = fork();
  if (child_pid < 0)  /* Error while forking */
    log_perror(LOG_WARNING, "couldn't fork");
  else if (child_pid == 0) {  // Child process
    /* We want to run the script in the background, without waiting for it to
       finish before continuing in the main process.  We also don't want to
       accumulate zombie processes.  So in this child process, we fork off
       _another_ child process to do the work and then exit immediately; the
       main process will receive our return value and continue running, and the
       child process will be inherited (and eventually reaped) by init. */
    pid_t const sub_child_pid = fork();
    if (sub_child_pid < 0)  // Error while forking
      log_perror(LOG_WARNING, "couldn't fork");
    else if (sub_child_pid > 0)  // Sub-parent process
      exit(0);
    else {  // Sub-child process
      execl(lease_script_filename,
	    lease_script_filename, lease_mode_strings[mode],
	    ip_string, ifn_internal, NULL);

      /* We only get here if something went wrong, so report the error */
      log_perror(LOG_ERR, "Couldn't exec lease script");
      exit(1);
    }
  }
  else  /* Parent process */
    waitpid(child_pid, NULL, 0);  /* Wait on sub-parent process;
				     sub-child continues independently */
}
