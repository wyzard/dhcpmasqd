/* interfaces.c - subsystem for sending and receiving DHCP packets
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <glib.h>
#include <libnet.h>
#include <pcap.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>

#include "interfaces.h"
#include "dhcp.h"
#include "leases.h"
#include "logging.h"
#include "packets.h"
#include "proxy.h"
#include "pcap-source.h"
#include "transactions.h"

typedef enum { DHCP_ROLE_CLIENT, DHCP_ROLE_SERVER } DhcpRole;

struct _Interface {
  pcap_t *pcap;
  libnet_t *libnet_context;
};

/* libpcap filter strings, corresponding to the DHCP_ROLE_CLIENT and
   DHCP_ROLE_SERVER values of the DhcpRole enumeration respectively:
   check source and dest ports, and reject fragmented packets and stray
   fragments */
static gchar const dhcp_client_filter_str[] =
  "(ip[6:2] & 0x3fff == 0) and (udp dst port bootpc and src port bootps)";
static gchar const dhcp_server_filter_str[] =
  "(ip[6:2] & 0x3fff == 0) and (udp dst port bootps and src port bootpc)";

/* The internal and external interfaces */
static Interface *interface_internal = NULL;
static Interface *interface_external = NULL;

static Interface *interface_create(gchar const *ifname, DhcpRole role);
static void interface_destroy(Interface *interface);

/* Open and close capture descriptors for receiving DHCP packets */
static pcap_t *create_pcap_descriptor(gchar const *ifname, DhcpRole role);
static void destroy_pcap_descriptor(pcap_t *pcap);

/* Open and close libnet contexts for injecting DHCP packets */
static libnet_t *create_libnet_context(gchar const *ifname);
static void destroy_libnet_context(libnet_t *libnet_context);

/* Handlers for received packets */
static gboolean pcap_internal_callback(GTimeVal const *now,
				       const struct pcap_pkthdr *header,
				       const guchar *body,
				       gpointer data);
static gboolean pcap_external_callback(GTimeVal const *now,
				       const struct pcap_pkthdr *header,
				       const guchar *body,
				       gpointer data);

/* interfaces_init()
 *
 * Initializes the networking subsystem
 */
void interfaces_init(gchar const *ifname_internal,
		     gchar const *ifname_external) {
  g_return_if_fail(ifname_internal != NULL);
  g_return_if_fail(ifname_external != NULL);
  g_return_if_fail(interface_internal == NULL);
  g_return_if_fail(interface_external == NULL);

  interface_internal = interface_create(ifname_internal, DHCP_ROLE_SERVER);
  interface_external = interface_create(ifname_external, DHCP_ROLE_CLIENT);

  pcap_source_add(interface_internal->pcap, pcap_internal_callback, NULL);
  pcap_source_add(interface_external->pcap, pcap_external_callback, NULL);
}

/* interfaces_cleanup()
 *
 * Tears down the networking subsystem
 */
void interfaces_cleanup() {
  interface_destroy(interface_internal);
  interface_destroy(interface_external);

  interface_internal = NULL;
  interface_external = NULL;
}

/* interface_transmit_frame()
 *
 * Transmits a frame on the network.
 */
void interface_transmit_frame(Interface const *interface,
			      GByteArray const *raw_frame) {
  g_return_if_fail(interface != NULL);
  g_return_if_fail(raw_frame != NULL);

  if (libnet_adv_write_link(interface->libnet_context,
			    raw_frame->data, raw_frame->len) < 0) {
    log_msg(LOG_ERR, "Couldn't send packet: %s",
	    libnet_geterror(interface->libnet_context));
  }
}

/* interface_create()
 *
 * Creates an Interface for communicating on a particular network interface.
 */
Interface *interface_create(gchar const *ifname, DhcpRole role) {
  g_return_val_if_fail(ifname != NULL, NULL);

  Interface *interface = g_slice_new(Interface);
  interface->pcap = create_pcap_descriptor(ifname, role);
  interface->libnet_context = create_libnet_context(ifname);

  return interface;
}

/* interface_destroy()
 *
 * Releases resources associated with an Interface.
 */
void interface_destroy(Interface *interface) {
  g_return_if_fail(interface != NULL);

  destroy_pcap_descriptor(interface->pcap);
  destroy_libnet_context(interface->libnet_context);
  g_slice_free(Interface, interface);
}

/* create_pcap_descriptor()
 *
 * Creates a capture descriptor to listen on an interface
 */
static pcap_t *create_pcap_descriptor(gchar const *ifname, DhcpRole role) {
  g_return_val_if_fail(ifname != NULL, NULL);

  gchar errbuf[PCAP_ERRBUF_SIZE] = "";

  /* Open the pcap capture descriptor for the requested interface */
  pcap_t *pcap = pcap_open_live(ifname, 65535, 0, 0, errbuf);
  if (G_UNLIKELY(pcap == NULL))
    log_fatal_msg(LOG_ERR, "Couldn't open capture descriptor on %s: %s",
		  1, ifname, errbuf);
  else if (errbuf[0] != '\0')  // Non-fatal warning
    log_msg(LOG_WARNING, "libpcap warning: %s", errbuf);

  /* Attach the appropriate packet filter to the socket */
  gchar const *dhcp_filter_str;
  if (role == DHCP_ROLE_CLIENT) dhcp_filter_str = dhcp_client_filter_str;
  else if (role == DHCP_ROLE_SERVER) dhcp_filter_str = dhcp_server_filter_str;
  else log_fatal_msg(LOG_ERR, "invalid DHCP role parameter in %s\n", 1,
		     __FUNCTION__);
  struct bpf_program dhcp_filter_bpf;
  if (pcap_compile(pcap, &dhcp_filter_bpf, dhcp_filter_str, 1, 0) < 0)
    log_fatal_msg(LOG_ERR, "Failed to compile packet filter: %s",
		  1, pcap_geterr(pcap));
  if (pcap_setfilter(pcap, &dhcp_filter_bpf) < 0)
    log_fatal_msg(LOG_ERR, "Failed to set packet filter: %s",
		  1, pcap_geterr(pcap));
  pcap_freecode(&dhcp_filter_bpf);

  return pcap;
}

/* destroy_pcap_descriptor()
 *
 * Destroys a capture descriptor created by create_pcap_descriptor()
 */
static void destroy_pcap_descriptor(pcap_t *pcap) {
  g_return_if_fail(pcap != NULL);

  pcap_close(pcap);
}

/* create_libnet_context()
 *
 * Creates an injection descriptor to transmit packets on an interface
 */
libnet_t *create_libnet_context(gchar const *ifname) {
  g_return_val_if_fail(ifname != NULL, NULL);

  gchar errbuf[LIBNET_ERRBUF_SIZE] = "";

  /* libnet_init()'s declaration is missing a const qualifier */
  gchar *libnet_ifname = (gchar *) ifname;

  /* Open the libnet descriptor for the requested interface */
  libnet_t *libnet_context =
    libnet_init(LIBNET_LINK_ADV, libnet_ifname, errbuf);
  if (G_UNLIKELY(libnet_context == NULL))
    log_fatal_msg(LOG_ERR, "Couldn't open libnet context on %s: %s",
		  1, ifname, errbuf);

  return libnet_context;
}

/* destroy_libnet_context()
 *
 * Destroys an injection descriptor created by create_libnet_context()
 */
static void destroy_libnet_context(libnet_t *libnet_context) {
  g_return_if_fail(libnet_context != NULL);

  libnet_destroy(libnet_context);
}

/* pcap_internal_callback()
 *
 * Called each time a DHCP packet is received on the internal interface
 */
static gboolean pcap_internal_callback(GTimeVal const *now,
				       const struct pcap_pkthdr *header,
				       const guchar *body,
				       gpointer data) {
  g_return_val_if_fail(interface_internal != NULL, FALSE);
  g_return_val_if_fail(interface_external != NULL, FALSE);

  if (G_LIKELY(header->caplen >= header-> len)) {
    GByteArray *raw_packet = g_byte_array_new();
    g_byte_array_append(raw_packet, body, header->len);

    GError *packet_create_error = NULL;
    DhcpPacket *packet = packet_create(raw_packet, &packet_create_error);

    if (G_LIKELY(packet != NULL)) {
      GError *proxy_error = NULL;
      proxy_outward(packet, interface_internal, interface_external,
		    now, &proxy_error);

      if (proxy_error != NULL) {
	log_msg(LOG_WARNING,
		"Failed to proxy packet outward: %s",
		proxy_error->message);
	g_error_free(proxy_error);
      }

      packet_destroy(packet);
    }
    else {
      log_msg(LOG_WARNING,
	      "Ignoring bad packet: %s", packet_create_error->message);
      g_error_free(packet_create_error);
    }
  }
  else
    log_msg(LOG_WARNING,
	    "Ignoring partial packet:  captured only %u of %u bytes",
	    header->caplen, header->len);

  return TRUE;
}

/* pcap_external_callback()
 *
 * Called each time a DHCP packet is received on the external interface
 */
static gboolean pcap_external_callback(GTimeVal const *now,
				       const struct pcap_pkthdr *header,
				       const guchar *body,
				       gpointer data) {
  g_return_val_if_fail(interface_internal != NULL, FALSE);
  g_return_val_if_fail(interface_external != NULL, FALSE);

  if (G_LIKELY(header->caplen >= header-> len)) {
    GByteArray *raw_packet = g_byte_array_new();
    g_byte_array_append(raw_packet, body, header->len);

    GError *packet_create_error = NULL;
    DhcpPacket *packet = packet_create(raw_packet, &packet_create_error);

    if (G_LIKELY(packet != NULL)) {
      GError *proxy_error = NULL;
      proxy_inward(packet, interface_external, interface_internal,
		   now, &proxy_error);

      if (proxy_error != NULL) {
	log_msg(LOG_WARNING,
		"Failed to proxy packet inward: %s",
		proxy_error->message);
	g_error_free(proxy_error);
      }

      packet_destroy(packet);
    }
    else {
      log_msg(LOG_WARNING,
	      "Ignoring bad packet: %s", packet_create_error->message);
      g_error_free(packet_create_error);
    }
  }
  else
    log_msg(LOG_WARNING,
	    "Ignoring partial packet:  captured only %u of %u bytes",
	    header->caplen, header->len);

  return TRUE;
}
