/* leases.c - subsystem for keeping track of outstanding DHCP leases
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <ctype.h>
#include <errno.h>
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "event-queue.h"
#include "event-source.h"
#include "leases.h"
#include "logging.h"
#include "routing.h"
#include "settings.h"
#include "util.h"

struct _DhcpLease {
  guint32 yiaddr;  // IP address assigned by lease
  GByteArray *client_id;  // Effective client identifier
  GTimeVal expiry;  // Absolute time when lease expires
};

/* The list of outstanding leases */
GList *dhcp_leases = NULL;

/* The queue of pending lease expiry events */
static EventQueue *event_queue_lease_expiry = NULL;

static void activate_lease(DhcpLease *lease);

/* Functions for working with the lease database */
static void read_lease_database(gchar const *leasedb_source_filename);
static void append_lease_database(DhcpLease const *lease);

/* Helper functions */
static gint lease_get_by_ipaddr_compare_func(gconstpointer a, gconstpointer b);
static void event_queue_lease_expiry_cleanup_callback(gpointer data,
						      gpointer user_data);
static gboolean event_source_lease_expiry_callback(gpointer data,
						   gpointer user_data);
static void refresh_lease_database_foreach_func(gpointer data,
						gpointer user_data);

/* Lease database currently being appended to */
FILE *leasedb;

/* leases_init()
 *
 * Initializes the leases subsystem
 */
void leases_init() {
  g_return_if_fail(event_queue_lease_expiry == NULL);
  event_queue_lease_expiry = event_queue_create();
  event_source_add(event_queue_lease_expiry,
		   event_source_lease_expiry_callback, NULL);

  /* Read in existing leases from the database file - this has the side effect
     of actually opening the file for writing */
  read_lease_database(leasedb_filename);
}

/* leases_cleanup()
 *
 * Tears down the leases subsystem
 */
void leases_cleanup() {
  g_return_if_fail(event_queue_lease_expiry != NULL);

  /* Flush the lease database to make sure any active leases are recorded */
  refresh_lease_database();

  /* Deactivate and free all active leases */
  event_queue_destroy(event_queue_lease_expiry,
		      event_queue_lease_expiry_cleanup_callback, NULL);
  event_queue_lease_expiry = NULL;
}

/* lease_get_by_ipaddr()
 *
 * Searches the outstanding-lease list for a lease with the given IP address;
 * returns a pointer to the lease, or NULL if none found
 */
DhcpLease *lease_get_by_ipaddr(guint32 client_ipaddr) {
  GList *lease_link =
    g_list_find_custom(dhcp_leases, GUINT_TO_POINTER(client_ipaddr),
		       lease_get_by_ipaddr_compare_func);

  if (lease_link)
    return lease_link->data;
  else return NULL;  // No match found
}

/* Helper function for lease_get_by_ipaddr() */
static gint lease_get_by_ipaddr_compare_func(gconstpointer a, gconstpointer b) {
  DhcpLease const *lease = a;
  guint32 client_ipaddr = GPOINTER_TO_UINT(b);

  if (lease->yiaddr == client_ipaddr) return 0;
  else if (lease->yiaddr < client_ipaddr) return -1;
  else if (lease->yiaddr > client_ipaddr) return +1;
  else g_assert_not_reached();
}

/* lease_begin()
 *
 * Creates a lease for a given IP and enables routing for it
 * This function takes ownership of the client_id GByteArray
 */
DhcpLease *lease_begin(guint32 client_ipaddr, GByteArray *client_id,
		       GTimeVal const *expiry) {
  g_return_val_if_fail(client_id != NULL, NULL);
  g_return_val_if_fail(expiry != NULL, NULL);

  DhcpLease *lease = g_slice_new(DhcpLease);
  lease->yiaddr = client_ipaddr;
  lease->client_id = client_id;
  lease->expiry = *expiry;

  activate_lease(lease);

  log_msg(LOG_NOTICE, "Created lease for address %u.%u.%u.%u\n",
	  (client_ipaddr >> 24) & 0xff,
	  (client_ipaddr >> 16) & 0xff,
	  (client_ipaddr >>  8) & 0xff,
	  (client_ipaddr)       & 0xff);

  return lease;
}

/* lease_renew()
 *
 * Updates the expiry time of a lease
 */
void lease_renew(DhcpLease *lease, GTimeVal const *expiry) {
  g_return_if_fail(lease != NULL);
  g_return_if_fail(expiry != NULL);

  /* Update the saved expiry time */
  lease->expiry = *expiry;

  /* Reschedule the expiry event */
  event_queue_remove_event(event_queue_lease_expiry, lease);
  event_queue_add_event(event_queue_lease_expiry, &lease->expiry, lease);

  /* Update the entry in the lease database */
  append_lease_database(lease);

  guint32 const client_ipaddr = lease->yiaddr;
  log_msg(LOG_NOTICE, "Renewed lease for address %u.%u.%u.%u\n",
	  (client_ipaddr >> 24) & 0xff,
	  (client_ipaddr >> 16) & 0xff,
	  (client_ipaddr >>  8) & 0xff,
	  (client_ipaddr)       & 0xff);
}

/* lease_expire()
 *
 * Releases resources used by a lease that has expired;
 * counterpart to lease_begin()
 *
 * This does *not* remove the lease from the expiry queue; it's assumed that's
 * been done already, such as by event_queue_get_expired_event().
 * Use lease_terminate() to terminate a lease that has not yet been removed
 * from the expiry queue.
 */
void lease_expire(DhcpLease *lease) {
  g_return_if_fail(lease != NULL);

  guint32 const client_ipaddr = lease->yiaddr;
  log_msg(LOG_NOTICE, "Expired lease for address %u.%u.%u.%u\n",
	  (client_ipaddr >> 24) & 0xff,
	  (client_ipaddr >> 16) & 0xff,
	  (client_ipaddr >>  8) & 0xff,
	  (client_ipaddr)       & 0xff);

  /* Deactivate and remove the lease */
  end_routing(client_ipaddr);
  dhcp_leases = g_list_remove(dhcp_leases, lease);
  if (lease->client_id != NULL)
    g_byte_array_free(lease->client_id, TRUE);
  g_slice_free(DhcpLease, lease);
}

/* lease_terminate()
 *
 * Terminates a lease prior to its natural expiry time;
 * alternative to lease_expire()
 */
void lease_terminate(DhcpLease *lease) {
  g_return_if_fail(lease != NULL);

  guint32 const client_ipaddr = lease->yiaddr;
  log_msg(LOG_NOTICE, "Forcing expiration of lease for address %u.%u.%u.%u\n",
	  (client_ipaddr >> 24) & 0xff,
	  (client_ipaddr >> 16) & 0xff,
	  (client_ipaddr >>  8) & 0xff,
	  (client_ipaddr)       & 0xff);

  /* Since the lease is being removed before its expiry time, change its
     expiry to zero and append the fact to the lease database, so it won't be
     activated again if we're restarted */
  lease->expiry.tv_sec  = 0;
  lease->expiry.tv_usec = 0;
  append_lease_database(lease);

  /* Remove the lease from the expiry queue, as would be done if it had
     actually expired */
  event_queue_remove_event(event_queue_lease_expiry, lease);

  /* The lease is effectively expired now so handle the expiration normally */
  lease_expire(lease);
}

/* event_queue_lease_expiry_cleanup_callback()
 *
 * Called for each existing lease during leases_cleanup()
 */
static void event_queue_lease_expiry_cleanup_callback(gpointer data,
						      gpointer user_data) {
  DhcpLease *lease = data;
  event_queue_remove_event(event_queue_lease_expiry, lease);

  /* Stop routing and clean up as if the lease had expired, even though it
     actually hasn't; it'll stay in the lease database and be read back in
     next time the program starts */
  /* TODO: Calling lease_expire() for a lease that's still valid is confusing;
     this could use some refactoring */
  lease_expire(lease);
}

/* event_source_lease_expiry_callback()
 *
 * Called by the main loop when a lease has expired
 */
static gboolean event_source_lease_expiry_callback(gpointer data,
						   gpointer user_data) {
  DhcpLease *lease = data;
  lease_expire(lease);

  return TRUE;
}

/* lease_get_client_id()
 *
 * Returns the client ID associated with a lease
 */
GByteArray const *lease_get_client_id(DhcpLease const *lease) {
  g_return_val_if_fail(lease != NULL, NULL);

  return lease->client_id;
}

/* lease_expiry()
 *
 * Returns the expiry time of a lease
 */
GTimeVal const *lease_expiry(DhcpLease const *lease) {
  g_return_val_if_fail(lease != NULL, NULL);

  return &lease->expiry;
}

/* activate_lease()
 */
void activate_lease(DhcpLease *lease) {
  /* Add the lease to the active list and schedule removal when it expires */
  dhcp_leases = g_list_append(dhcp_leases, lease);
  event_queue_add_event(event_queue_lease_expiry, &lease->expiry, lease);

  /* Record it to the lease database to persist across restarts */
  append_lease_database(lease);

  /* Begin routing for the IP address granted by this lease */
  guint32 const client_ipaddr = lease->yiaddr;
  begin_routing(client_ipaddr);
}

/* read_lease_database()
 *
 * Reads the contents of a lease-database file into the active-leases list
 */
void read_lease_database(gchar const *leasedb_source_filename) {
  GList *new_dhcp_leases = NULL;

  FILE *leasedb_source = fopen(leasedb_source_filename, "r");
  if (leasedb_source == NULL) {
    log_perror(LOG_WARNING, "Couldn't open lease database for reading");
    log_msg(LOG_WARNING, "Starting with an empty set of leases");
  }
  else {
    /* Read each lease from the file and build a temporary list, unsorted, of
       the most recent definition for each lease */
    unsigned int linenum = 0;
    while (!feof(leasedb_source)) {
      struct {
	guint ip_bytes[4];
	guint expiry_sec;
	guint expiry_usec;
	gchar client_id_str[255+1];  // DHCP options are limited to 255 bytes
      } leasedata;

      gint scanresult = fscanf(leasedb_source,
			       "%u.%u.%u.%u %u %u %255s\n",
			       &leasedata.ip_bytes[3], &leasedata.ip_bytes[2],
			       &leasedata.ip_bytes[1], &leasedata.ip_bytes[0],
			       &leasedata.expiry_sec,  &leasedata.expiry_usec,
			        leasedata.client_id_str);

      if (scanresult == EOF) break;
      ++linenum;

      if (scanresult < 7) {
	log_msg(LOG_WARNING,
		"Error reading line %u from %s; ignoring this lease",
		linenum, leasedb_source_filename);
	fscanf(leasedb_source, "%*s\n");  // Eat chars up to the next newline
	continue;
      }

      /* Check that the IP components are less than 256, and build the IP */
      /* (Rather than comparing against 256 ten times, we OR all the bits
	 together and check whether any of them were past the eighth bit */
      /* No need to check the hardware-address components - they're hex values
	 limited to 2 digits, so they're necessarily less than 256 */
      if ((leasedata.ip_bytes[0] | leasedata.ip_bytes[1] |
	   leasedata.ip_bytes[2] | leasedata.ip_bytes[3]) >= 256) {
	log_msg(LOG_WARNING,
		"Invalid IP address on line %u of %s; ignoring this lease",
		linenum, leasedb_source_filename);
	continue;
      }
      guint32 temp_ip = (guint32)(leasedata.ip_bytes[3] << 24 |
				  leasedata.ip_bytes[2] << 16 |
				  leasedata.ip_bytes[1] << 8  |
				  leasedata.ip_bytes[0]);

      /* Build the client identifier from the hex string */
      GString *client_id_hex = g_string_new(leasedata.client_id_str);
      GError *hex_to_bytes_error = NULL;
      GByteArray *temp_client_id =
	hex_to_bytes(client_id_hex, &hex_to_bytes_error);
      g_string_free(client_id_hex, TRUE);

      if (temp_client_id == NULL) {  // Conversion failed
	g_assert(hex_to_bytes_error != NULL);
	log_msg(LOG_WARNING,
		"Failed to read client ID on line %u of %s: %s",
		linenum, leasedb_source_filename,
		hex_to_bytes_error->message);
	g_error_free(hex_to_bytes_error);
	continue;
      }

      /* Search the temporary list for an existing lease on this IP, and
	 overwrite its hardware address and expiry time with the new one */
      GList *found_lease =
	g_list_find_custom(new_dhcp_leases, GUINT_TO_POINTER(temp_ip),
			   lease_get_by_ipaddr_compare_func);
      if (found_lease) {
	DhcpLease *lease = found_lease->data;
	(lease->expiry).tv_sec  = leasedata.expiry_sec;
	(lease->expiry).tv_usec = leasedata.expiry_usec;
	if (lease->client_id != NULL)
	  g_byte_array_free(lease->client_id, TRUE);
	lease->client_id = temp_client_id;
      }
      else {  // No existing lease on this IP was found, so create a new one
	DhcpLease *lease = g_slice_new(DhcpLease);
	lease->yiaddr = temp_ip;
	lease->expiry =
	  ((GTimeVal) { leasedata.expiry_sec, leasedata.expiry_usec });
	lease->client_id = temp_client_id;
	new_dhcp_leases = g_list_append(new_dhcp_leases, lease);
      }
    }

    fclose(leasedb_source);
  }

  /* "Refresh" the lease database, even though the current lease list is still
     empty, in order to get the file opened for writing */
  refresh_lease_database();

  /* We now have a list containing the most recent lease definition for each
     lease in the file; find the ones that aren't expired and add them
     for real - this activates them and also adds them to the just-emptied
     database file */
  GTimeVal now;
  g_get_current_time(&now);
  GList *iter = g_list_first(new_dhcp_leases);
  while (iter != NULL) {
    DhcpLease *lease = iter->data;

    /* If the lease isn't expired, activate it */
    if (timecmp(&(lease->expiry), &now) > 0) {
      activate_lease(lease);
    }
    /* If the lease is expired but still in the list, we might have stopped
       running before it expired, so make sure we're not routing for it
       anymore */
    else {
      end_routing(lease->yiaddr);

      if (lease->client_id != NULL)
	g_byte_array_free(lease->client_id, TRUE);
      g_slice_free(DhcpLease, lease);
    }

    iter = g_list_next(iter);
  }

  /* Everything in the new_dhcp_leases list has been either freed or added
     to the real dhcp_leases list, so free the list */
  g_list_free(new_dhcp_leases);
}

/* append_lease_database()
 *
 * Writes a line representing a lease to the lease database
 */
void append_lease_database(DhcpLease const *lease) {
  if (leasedb == NULL) return;

  GString *client_id_hex = bytes_to_hex(lease->client_id);
  fprintf(leasedb, "%u.%u.%u.%u %u %u %.*s\n",
	  (lease->yiaddr >> 24) & 0xff,
	  (lease->yiaddr >> 16) & 0xff,
	  (lease->yiaddr >>  8) & 0xff,
	  (lease->yiaddr)       & 0xff,
	  (guint32)((lease->expiry).tv_sec),
	  (guint32)((lease->expiry).tv_usec),
	  (int) client_id_hex->len, client_id_hex->str);
  g_string_free(client_id_hex, TRUE);
}

/* refresh_lease_database()
 *
 * Replaces the lease database with a fresh copy generated from the current
 * active list
 *
 * Currently this truncates and overwrites the existing lease database
 * in-place, so there is a brief window of time during which there are no leases
 * in the database file, and a crash would cause the set of leases to be
 * forgotten.
 */
void refresh_lease_database() {
  /* Truncate and re-open the lease database */
  if (leasedb != NULL) fclose(leasedb);
  leasedb = fopen(leasedb_filename, "w");
  if (leasedb == NULL) {
    log_msg(LOG_ALERT, "Couldn't reopen lease database %s for refreshing: %s\n",
	    leasedb_filename, strerror(errno));
    /* Process will continue, but without a lease database */
    return;
  }

  /* Set the file line-buffered so leases are immediately written to disk as
     they are added */
  setlinebuf(leasedb);

  /* Write the currently-active leases to the file */
  g_list_foreach(dhcp_leases, refresh_lease_database_foreach_func, NULL);
}

/* Helper function for refresh_lease_database() */
static void refresh_lease_database_foreach_func(gpointer data,
						gpointer user_data) {
  DhcpLease const *lease = data;
  append_lease_database(lease);
}
