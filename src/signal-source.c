/* signal-source.c - GSource to bind libdaemon's signal queue to the GLib
 *                   main loop
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <glib.h>
#include <libdaemon/dsignal.h>

#include "signal-source.h"

typedef struct _SignalSource SignalSource;
struct _SignalSource {
  GSource source;

  /* Used by the main loop's polling */
  GPollFD poll_fd;
};

/* Static functions */
static gboolean signal_source_prepare(GSource *source, gint *timeout);
static gboolean signal_source_check(GSource *source);
static gboolean signal_source_dispatch(GSource *source,
				       GSourceFunc callback,
				       gpointer user_data);
static void signal_source_finalize(GSource *source);

/* Singleton instance of the source -- only one may exist at a time */
static GSource *the_source = NULL;

GSourceFuncs signal_source_funcs = {
  signal_source_prepare,
  signal_source_check,
  signal_source_dispatch,
  signal_source_finalize,
  NULL,  /* closure_callback */
  NULL,  /* closure_marshal */
};

gint signal_source_handle_signal(gint const signal) {
  return daemon_signal_install(signal);
}

GSource *signal_source_new() {
  /* Abort if there's already a live instance of the source */
  g_return_val_if_fail(the_source == NULL, NULL);

  the_source =
    g_source_new(&signal_source_funcs, sizeof(SignalSource));
  SignalSource *signal_source = (SignalSource *) the_source;

  /* Initialize the structure members */
  signal_source->poll_fd.fd = daemon_signal_fd();
  signal_source->poll_fd.events = G_IO_IN | G_IO_HUP | G_IO_ERR;

  /* Safety check in case daemon_signal_fd() fails */
  g_return_val_if_fail(signal_source->poll_fd.fd != -1, NULL);

  /* Tell the main loop to poll the signal queue for activity */
  g_source_add_poll(the_source, &signal_source->poll_fd);

  return the_source;
}

guint signal_source_add(SignalSourceFunc callback, gpointer user_data) {
  return signal_source_add_full(callback, user_data, NULL);
}

guint signal_source_add_full(SignalSourceFunc callback, gpointer user_data, GDestroyNotify notify) {
  g_return_val_if_fail(callback != NULL, 0);

  GSource *source = signal_source_new();
  g_return_val_if_fail(source != NULL, 0);

  /* Register the given callback with the source. */
  g_source_set_callback(source, (GSourceFunc) callback, user_data, notify);

  /* Add the source to the default main context. */
  guint const attach_id = g_source_attach(source, NULL);

  /* Release this function's reference to the source; it remains referenced
   * by the GMainContext to which it's attached. */
  g_source_unref(source);

  return attach_id;
}

gboolean signal_source_prepare(GSource *source, gint *timeout) {
  g_return_val_if_fail(source != NULL, FALSE);
  g_return_val_if_fail(timeout != NULL, FALSE);

  g_assert(source == the_source);

  *timeout = -1;

  return FALSE;
}

gboolean signal_source_check(GSource *source) {
  g_return_val_if_fail(source != NULL, FALSE);

  g_assert(source == the_source);

  SignalSource *signal_source = (SignalSource *) source;

  /* This was established in signal_source_new() */
  g_assert(signal_source->poll_fd.fd == daemon_signal_fd());

  return (signal_source->poll_fd.revents != 0);
}

gboolean signal_source_dispatch(GSource *source,
				GSourceFunc callback,
				gpointer user_data) {
  g_return_val_if_fail(source != NULL, FALSE);
  g_return_val_if_fail(callback != NULL, FALSE);

  g_assert(source == the_source);

  SignalSource const *signal_source = (SignalSource *) source;
  SignalSourceFunc signal_callback = (SignalSourceFunc) callback;

  gint signal = daemon_signal_next();
  if (G_LIKELY(signal > 0))  // Successfully received a signal
    signal_callback(signal, user_data);
  else if (signal == 0) {  // No signal was queued
    g_warning("SignalSource dispatcher called when no signal was queued");
  }
  else {  // daemon_signal_next() failed
    g_warning("Failed to receive signal");
  }

  if (G_UNLIKELY(signal_source->poll_fd.revents & (G_IO_HUP | G_IO_ERR)))
    return FALSE;  // No more input will come from this source
  else
    return TRUE;  // Main loop should continue checking this source
}

void signal_source_finalize(GSource *source) {
  g_return_if_fail(source != NULL);

  g_assert(source == the_source);

  /* Clean up libdaemon's signal handler */
  daemon_signal_done();

  /* Reset the singleton pointer */
  the_source = NULL;
}
