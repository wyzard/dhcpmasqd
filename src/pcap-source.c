/* pcap-source.c - GSource to bind libpcap to the GLib main loop
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <glib.h>
#include <pcap.h>

#include "pcap-source.h"

typedef struct _PcapSource PcapSource;
struct _PcapSource {
  GSource source;

  /* The PCap capture descriptor */
  pcap_t *pcap;

  /* Used by the main loop's polling */
  GPollFD poll_fd;
};

/* Static functions */
static gboolean pcap_source_prepare(GSource *source, gint *timeout);
static gboolean pcap_source_check(GSource *source);
static gboolean pcap_source_dispatch(GSource *source,
				     GSourceFunc callback,
				     gpointer user_data);

GSourceFuncs pcap_source_funcs = {
  pcap_source_prepare,
  pcap_source_check,
  pcap_source_dispatch,
  NULL,  // finalize
  NULL,  /* closure_callback */
  NULL,  /* closure_marshal */
};

GSource *pcap_source_new(pcap_t *pcap) {
  g_return_val_if_fail(pcap != NULL, NULL);

  GSource *source =
    g_source_new(&pcap_source_funcs, sizeof(PcapSource));
  PcapSource *pcap_source = (PcapSource *) source;

  /* Initialize the structure members */
  pcap_source->pcap = pcap;
  pcap_source->poll_fd.fd = pcap_fileno(pcap_source->pcap);
  pcap_source->poll_fd.events = G_IO_IN | G_IO_HUP | G_IO_ERR;

  /* Safety check in case pcap_fileno() fails */
  g_return_val_if_fail(pcap_source->poll_fd.fd != -1, NULL);

  /* Tell the main loop to poll the capture descriptor for activity */
  g_source_add_poll(source, &pcap_source->poll_fd);

  return source;
}

guint pcap_source_add(pcap_t *pcap, PcapSourceFunc callback, gpointer user_data) {
  return pcap_source_add_full(pcap, callback, user_data, NULL);
}

guint pcap_source_add_full(pcap_t *pcap, PcapSourceFunc callback, gpointer user_data, GDestroyNotify notify) {
  g_return_val_if_fail(pcap != NULL, 0);
  g_return_val_if_fail(callback != NULL, 0);

  GSource *source = pcap_source_new(pcap);

  /* Register the given callback with the source. */
  g_source_set_callback(source, (GSourceFunc) callback, user_data, notify);

  /* Add the source to the default main context. */
  guint const attach_id = g_source_attach(source, NULL);

  /* Release this function's reference to the source; it remains referenced
   * by the GMainContext to which it's attached. */
  g_source_unref(source);

  return attach_id;
}

gboolean pcap_source_prepare(GSource *source, gint *timeout) {
  g_return_val_if_fail(source != NULL, FALSE);
  g_return_val_if_fail(timeout != NULL, FALSE);

  *timeout = -1;

  return FALSE;
}

gboolean pcap_source_check(GSource *source) {
  g_return_val_if_fail(source != NULL, FALSE);

  PcapSource *pcap_source = (PcapSource *) source;

  /* This was established in pcap_source_new() */
  g_assert(pcap_source->poll_fd.fd == pcap_fileno(pcap_source->pcap));

  return (pcap_source->poll_fd.revents != 0);
}

gboolean pcap_source_dispatch(GSource *source,
			      GSourceFunc callback,
			      gpointer user_data) {
  g_return_val_if_fail(source != NULL, FALSE);
  g_return_val_if_fail(callback != NULL, FALSE);

  PcapSource const *pcap_source = (PcapSource *) source;
  PcapSourceFunc pcap_callback = (PcapSourceFunc) callback;

  GTimeVal now;
  g_source_get_current_time(source, &now);

  if (G_LIKELY(pcap_source->poll_fd.revents & G_IO_IN)) {  // Input is ready
    /* Get the next packet from the wire */
    /* TODO: Loop to read multiple packets? */
    struct pcap_pkthdr *pkt_header;
    guchar const *pkt_data;
    gint result =
      pcap_next_ex(pcap_source->pcap, &pkt_header, &pkt_data);

    if (G_LIKELY(result))
      pcap_callback(&now, pkt_header, pkt_data, user_data);
    else
      g_warning("Failed to read packet: %s", pcap_geterr(pcap_source->pcap));
  }

  if (G_UNLIKELY(pcap_source->poll_fd.revents & (G_IO_HUP | G_IO_ERR)))
    return FALSE;  // No more input will come from this source
  else
    return TRUE;  // Main loop should continue checking this source
}
