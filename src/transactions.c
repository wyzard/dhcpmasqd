/* transactions.c - subsystem for keeping track of ongoing DHCP transactions
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <glib.h>
#include <string.h>
#include "event-queue.h"
#include "event-source.h"
#include "settings.h"

#include "transactions.h"

struct _DhcpTransaction {
  guint32 xid;  // Transaction ID from DHCP packet
  GByteArray *client_hwaddr;  // Client's hardware (e.g. MAC) address
  GByteArray *client_id;  // Effective client identifier
  gboolean broadcast_flag;  // If true, responses should be sent via broadcast
};

/* Static functions */
static gint transaction_get_by_xid_compare_func(gconstpointer a, gconstpointer b);
static void transaction_terminate(DhcpTransaction *transaction);
static void event_queue_transaction_expiry_cleanup_callback(gpointer data,
							    gpointer user_data);
static gboolean event_source_transaction_expiry_callback(gpointer data,
							 gpointer user_data);

/* The list of active transactions */
static GSList *active_transaction_list = NULL;

/* The queue of pending transaction expiry events */
static EventQueue *event_queue_transaction_expiry = NULL;

/* transactions_init()
 *
 * Initializes the transactions subsystem
 */
void transactions_init() {
  g_return_if_fail(event_queue_transaction_expiry == NULL);
  event_queue_transaction_expiry = event_queue_create();
  event_source_add(event_queue_transaction_expiry,
		   event_source_transaction_expiry_callback, NULL);
}

/* transactions_cleanup()
 *
 * Tears down the transactions subsystem
 */
void transactions_cleanup() {
  g_return_if_fail(event_queue_transaction_expiry != NULL);
  event_queue_destroy(event_queue_transaction_expiry,
		      event_queue_transaction_expiry_cleanup_callback, NULL);
  event_queue_transaction_expiry = NULL;
}

/* transaction_get_by_xid()
 *
 * Searches the active-transaction list for a transaction with the given ID;
 * returns a pointer to the transaction, or NULL if none found
 */
DhcpTransaction *transaction_get_by_xid(guint32 xid) {
  GSList *transaction_link =
    g_slist_find_custom(active_transaction_list,
			GUINT_TO_POINTER(xid),
			transaction_get_by_xid_compare_func);
  if (transaction_link) {
    return transaction_link->data;
  }
  else return NULL;
}

/* Helper for transaction_get_by_xid() */
static gint transaction_get_by_xid_compare_func(gconstpointer a, gconstpointer b) {
  DhcpTransaction const *transaction = a;
  guint32 xid = GPOINTER_TO_UINT(b);

  g_return_val_if_fail(transaction != NULL, -1);  // Return anything but 0

  if (transaction->xid == xid) return 0;
  else if (transaction->xid < xid) return -1;
  else if (transaction->xid > xid) return +1;
  else g_assert_not_reached();
}

/* transaction_begin()
 *
 * Creates a record for a new transaction
 * client_id is optional; use NULL if the client didn't specify an ID
 * This function takes ownership of the GByteArrays given
 */
DhcpTransaction *transaction_begin(guint32 xid,
				   GByteArray *client_hwaddr,
				   GByteArray *client_id,
				   gboolean broadcast_flag,
				   GTimeVal const *now) {
  g_return_val_if_fail(client_hwaddr != NULL, NULL);
  g_return_val_if_fail(client_id != NULL, NULL);
  g_return_val_if_fail(now != NULL, NULL);

  DhcpTransaction *transaction = g_slice_new(DhcpTransaction);

  /* Initialize the fixed attributes */
  transaction->xid = xid;
  transaction->client_hwaddr = client_hwaddr;
  transaction->client_id = client_id;
  transaction->broadcast_flag = broadcast_flag;

  /* Add the transaction to the active list */
  active_transaction_list =
    g_slist_append(active_transaction_list, transaction);

  /* Add expiry event for transaction */
  GTimeVal transaction_expiry_time;
  transaction_expiry_time.tv_sec  = now->tv_sec + transaction_timeout;
  transaction_expiry_time.tv_usec = now->tv_usec;
  event_queue_add_event(event_queue_transaction_expiry,
			&transaction_expiry_time, transaction);

  return transaction;
}

/* transaction_continue()
 *
 * Acknowledges that there has been further activity in a transaction
 */
void transaction_continue(DhcpTransaction *transaction, GTimeVal const *now) {
  g_return_if_fail(transaction != NULL);
  g_return_if_fail(now != NULL);

  /* Remove old expiry event for transaction */
  event_queue_remove_event(event_queue_transaction_expiry, transaction);

  /* Add new expiry event for transaction */
  GTimeVal transaction_expiry_time;
  transaction_expiry_time.tv_sec  = now->tv_sec + transaction_timeout;
  transaction_expiry_time.tv_usec = now->tv_usec;
  event_queue_add_event(event_queue_transaction_expiry,
			&transaction_expiry_time, transaction);
}

/* transaction_terminate()
 *
 * Releases resources used by a transaction;
 * counterpart to transaction_begin()
 */
void transaction_terminate(DhcpTransaction *transaction) {
  g_return_if_fail(transaction != NULL);

  active_transaction_list =
    g_slist_remove(active_transaction_list, transaction);
  g_byte_array_free(transaction->client_hwaddr, TRUE);
  g_byte_array_free(transaction->client_id, TRUE);
  g_slice_free(DhcpTransaction, transaction);
}

/* event_queue_transaction_expiry_cleanup_callback()
 *
 * Called for each existing transaction during transactions_cleanup()
 */
static void event_queue_transaction_expiry_cleanup_callback(gpointer data,
							    gpointer user_data) {
  DhcpTransaction *transaction = data;
  transaction_terminate(transaction);
}

/* event_source_transaction_expiry_callback()
 *
 * Called by the main loop when a transaction has expired
 */
static gboolean event_source_transaction_expiry_callback(gpointer data,
							 gpointer user_data) {
  DhcpTransaction *transaction = data;
  transaction_terminate(transaction);

  return TRUE;
}

GByteArray const *transaction_client_hwaddr(DhcpTransaction const *transaction) {
  g_return_val_if_fail(transaction != NULL, NULL);
  return transaction->client_hwaddr;
}

GByteArray const *transaction_get_client_id(DhcpTransaction const *transaction) {
  g_return_val_if_fail(transaction != NULL, NULL);

  return transaction->client_id;
}

gboolean transaction_get_broadcast_flag(DhcpTransaction const *transaction) {
  g_return_val_if_fail(transaction != NULL, FALSE);

  return transaction->broadcast_flag;
}
