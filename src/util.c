/* packets.c - miscellaneous utility functions
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <glib.h>
#include <stdio.h>
#include <string.h>

#include "util.h"

GQuark dhcpmasqd_hex_to_bytes_error_quark() {
  return g_quark_from_static_string("dhcpmasqd-hex-to-bytes-error-quark");
}

GQuark dhcpmasqd_bytes_to_var_error_quark() {
  return g_quark_from_static_string("dhcpmasqd-bytes-to-var-error-quark");
}

/* bytes_to_hex()
 *
 * Converts an array of bytes into a string of hex digits.
 * The caller is responsible for freeing the returned GString.
 */
GString *bytes_to_hex(GByteArray const *bytes) {
  g_return_val_if_fail(bytes != NULL, NULL);

  GString *hex = g_string_sized_new(bytes->len * 2);
  for (guint i = 0; i < bytes->len; i += 1) {
    g_string_append_printf(hex, "%02x", bytes->data[i]);
  }

  return hex;
}

/* hex_to_bytes()
 *
 * Converts a string of hex digits into an array of bytes.
 * The caller is responsible for freeing the returned GByteArray.
 *
 * If the string is malformed, an error will be reported via the GError
 * parameter, and NULL will be returned.
 */
GByteArray *hex_to_bytes(GString const *hex, GError **error) {
  g_return_val_if_fail(hex != NULL, NULL);
  g_return_val_if_fail(error == NULL || *error == NULL, NULL);

  /* Hex strings must contain an even number of digits */
  if ((hex->len % 2) != 0) {
    g_set_error(error,
		DHCPMASQD_HEX_TO_BYTES_ERROR,
		DHCPMASQD_HEX_TO_BYTES_ERROR_ODD_LENGTH,
		"hex string \"%.*s\" contains an odd number of digits",
		(int) hex->len, hex->str);
    return NULL;
  }

  GByteArray *bytes = g_byte_array_sized_new(hex->len / 2);
  for (guint i = 0; i < hex->len; i += 2) {
    guint value;
    gint chars_read = 0;
    int scan_result = sscanf(hex->str + i, "%02x%n", &value, &chars_read);

    if (scan_result == EOF || chars_read != 2) {  // Invalid or truncated input
      g_byte_array_free(bytes, TRUE);
      g_set_error(error,
		  DHCPMASQD_HEX_TO_BYTES_ERROR,
		  DHCPMASQD_HEX_TO_BYTES_ERROR_INVALID_HEX,
		  "hex string \"%.*s\" contains invalid characters",
		  (int) hex->len, hex->str);
      return NULL;
    }

    /* Only two hex digits were read, so the result must fit in 8 bits */
    g_assert(value <= G_MAXUINT8);

    guint8 byte = value;
    g_byte_array_append(bytes, &byte, 1);
  }

  return bytes;
}

/* bytes_to_var_impl()
 *
 * Copies a value from a GByteArray into a specified variable.  The length of
 * the byte array must match the size of the variable, or the mismatch will be
 * reported as an error.
 *
 * This function should typically be invoked via the bytes_to_var() macro,
 * which allows the caller to specify just the name of the target variable
 * rather than its address and size.
 */
void bytes_to_var_impl(GByteArray const *bytes, void *var_ptr, gsize var_size,
		       GError **error) {
  g_return_if_fail(bytes != NULL);
  g_return_if_fail(var_ptr != NULL && var_size > 0);
  g_return_if_fail(error == NULL || *error == NULL);

  if (G_LIKELY(bytes->len == var_size)) {
    memcpy(var_ptr, bytes->data, var_size);
  }
  else {
    g_set_error(error,
		DHCPMASQD_BYTES_TO_VAR_ERROR,
		DHCPMASQD_BYTES_TO_VAR_ERROR_WRONG_SIZE,
		"expected %" G_GSIZE_FORMAT " bytes, "
		"got %u", var_size, bytes->len);
  }
}
