/* event-source.c - GSource to bind an event queue to the GLib main loop
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <glib.h>

#include "event-queue.h"
#include "event-source.h"

typedef struct _EventSource EventSource;
struct _EventSource {
  GSource source;

  /* The event queue from which we're pulling events */
  EventQueue *event_queue;
};

/* Static functions */
static gboolean event_source_prepare(GSource *source, gint *timeout);
static gboolean event_source_check(GSource *source);
static gboolean event_source_dispatch(GSource *source,
				      GSourceFunc callback,
				      gpointer user_data);

GSourceFuncs event_source_funcs = {
  event_source_prepare,
  event_source_check,
  event_source_dispatch,
  NULL,  // finalize
  NULL,  /* closure_callback */
  NULL,  /* closure_marshal */
};

GSource *event_source_new(EventQueue *event_queue) {
  g_return_val_if_fail(event_queue != NULL, NULL);

  GSource *source =
    g_source_new(&event_source_funcs, sizeof(EventSource));
  EventSource *event_source = (EventSource *) source;

  event_source->event_queue = event_queue;

  return source;
}

guint event_source_add(EventQueue *event_queue, EventSourceFunc callback, gpointer user_data) {
  return event_source_add_full(event_queue, callback, user_data, NULL);
}

guint event_source_add_full(EventQueue *event_queue, EventSourceFunc callback, gpointer user_data, GDestroyNotify notify) {
  g_return_val_if_fail(event_queue != NULL, 0);
  g_return_val_if_fail(callback != NULL, 0);

  GSource *source = event_source_new(event_queue);

  /* Register the given callback with the source. */
  g_source_set_callback(source, (GSourceFunc) callback, user_data, notify);

  /* Add the source to the default main context. */
  guint const attach_id = g_source_attach(source, NULL);

  /* Release this function's reference to the source; it remains referenced
   * by the GMainContext to which it's attached. */
  g_source_unref(source);

  return attach_id;
}

gboolean event_source_prepare(GSource *source, gint *timeout) {
  g_return_val_if_fail(source != NULL, FALSE);
  g_return_val_if_fail(timeout != NULL, FALSE);

  EventSource *event_source = (EventSource *) source;

  GTimeVal const *next_event_time =
    event_queue_next_event_time(event_source->event_queue);
  if (next_event_time == NULL) {  // No events in the queue
    *timeout = -1;
    return FALSE;
  }
  else {
    GTimeVal now;
    g_source_get_current_time(source, &now);

    if (timecmp(&now, next_event_time) >= 0) {  // An event is already expired
      *timeout = 0;
      return TRUE;
    }
    else {  // No expired events; figure out how long to wait until the next one
      GTimeVal delay;
      timediff(&delay, next_event_time, &now);
      *timeout = (delay.tv_sec * 1000) + (delay.tv_usec / 1000);
      return FALSE;
    }
  }
}

gboolean event_source_check(GSource *source) {
  g_return_val_if_fail(source != NULL, FALSE);

  EventSource *event_source = (EventSource *) source;

  GTimeVal const *next_event_time =
    event_queue_next_event_time(event_source->event_queue);
  if (next_event_time == NULL)  // No events in the queue
    return FALSE;
  else {
    GTimeVal now;
    g_source_get_current_time(source, &now);

    if (timecmp(&now, next_event_time) >= 0)
      return TRUE;  // An event is expired, so handle it now
    else
      return FALSE;  // No events expired
  }
}

gboolean event_source_dispatch(GSource *source,
				GSourceFunc callback,
				gpointer user_data) {
  g_return_val_if_fail(source != NULL, FALSE);
  g_return_val_if_fail(callback != NULL, FALSE);

  EventSource const *event_source = (EventSource *) source;
  EventSourceFunc event_callback = (EventSourceFunc) callback;

  GTimeVal now;
  g_source_get_current_time(source, &now);

  gpointer expired_event_data;
  while ((expired_event_data =
	  event_queue_get_expired_event(event_source->event_queue, &now))
	 != NULL) {  // For each expired event
    event_callback(expired_event_data, user_data);
  }

    return TRUE;  // Main loop should continue checking this source
}
