/* logging.c - subsystem for writing diagnostic and error messages
 * Copyright (C) 2009  Michael B. Paul
 *
 * This file is part of dhcpmasqd.
 *
 * dhcpmasqd is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * dhcpmasqd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * dhcpmasqd.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <libdaemon/dlog.h>
#include <errno.h>
#include <glib.h>
#include <stdarg.h>  // for va_start()/va_end()
#include <string.h>  // for sterror()
#include <stdlib.h>  // for exit()

#include "logging.h"

/* Functions for writing messages to syslog.
 *
 * The "_perror" functions write the supplied message followed by a colon, a
 *   space, and the error string associated with the "errno" variable, in the
 *   same way perror() does.
 * The "_msg" functions allow the caller to provide the format string and
 *   its arguments.
 * The "_fatal" variants write the message and then terminate the program with
 *   the supplied exit status.
 */

void log_perror(gint priority, gchar const *prefix) {
  daemon_log(priority, "%s: %s", prefix, strerror(errno));
}

void log_msg(gint priority, gchar const *format, ...) {
  va_list args;

  va_start(args, format);
  daemon_logv(priority, format, args);
  va_end(args);
}

void log_fatal_perror(gint priority, gchar const *prefix, gint status) {
  log_perror(priority, prefix);
  exit(status);
}

void log_fatal_msg(gint priority, gchar const *format, gint status,  ...) {
  va_list args;

  va_start(args, status);
  daemon_logv(priority, format, args);
  va_end(args);
  exit(status);
}
